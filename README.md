# sp00tn1k client

**[Robot]** | **[Base]** | **[UI]** | **[Hardware]** | **[Peer Server]**

This repository contains the client part of the sp00tn1k robotic telepresence system.

## Location

Current server address: https://www.sp00tn1k.org

## Getting Started

sp00tn1k client is developed in vanilla JavaScript ES6, with minimal use of frameworks, to make it easy for anyone with basic JS knowledge to work with the system.
The development is done with node.js (stable branch). The recommended way to install the development environment is by using NVM (https://github.com/nvm-sh/nvm). The client is built using parcel.

After the environment is set up, clone this repository, then run in console:

```bash
npm install
npm run build
```

This will build the client software into a dist directory. Alternatively, `npm run serve` will spawn a web server at https://localhost:1234. Limited testing is provided by `npm run test`.

## Usage

Two main usage scenarios of sp00tn1k client include web application and native application. Web application is the main development target, while native application is built using nw.js framework.

### As a Web Application

Even though the sp00tn1k client is expected to work flawlessly in a majority of modern browsers, we recommend using Mozilla Firefox for best security and user experience. 
In particular testing and development of the VR mode was done primarily on Firefox. Chrome and Edge may work with most features.

Note about XR support: The WebXR Device API is under development. Please follow the browser compatibility guidelines for further information.

### As a Native Application using Electron Framework

nw.js is used to run application in native mode. First, compile the app with `npm run build`. Then nw can be started with `npm run start`.

## Coding Conventions

sp00tn1k client is written in vanilla JavaScript, using ES6 standard. For example, we use `import` in place of `require`. ESLint is set to standard settings.

Do not re-use elements in the UI. If a plugin needs to have a button or a field, create it and then use exclusively.

## Plugin System

The client itself only provides essentials, such as peering and media channels, whereas most of the functionality is provided using plugins (located in ./src/js/plugins/). These plugins can both receive information from and send commands to the robot. All connection with the robot and its UI (aside from the video and audio streams) is sent as ROS messages. It should be noted that the client software does NOT perform any consistency checks on what is being sent. E.g., if two plugins send conflicting messages on the same topic, everything will be delivered to the robot.
Every plugin inherits from the Plugin base class. When instantiated, every plugin receives references to the UI and ROS, even if they are not used in the plugin logic. 
Some of the plugins operate in conjunction with (and rely on) a corresponding node on the robot's side.

Robot-specific configuration parameters are sent to the UI and can be used by the plugins and renderer (e.g., velocity limits, camera properties, etc.)

### Provided plugins

#### Battery
This well-commented plugin just subscribes to a battery topic and shows the current battery voltage in the UI. Use it as an example of how to make a simple subscriber.

#### MouseDriver
This plugin implements a simple mouse driver, which allows to steer the robot using the mouse cursor or touch gestures.
In VR mode, the VR controller's touchpad serves as the input source.

#### Velocity
Shows the robot's current linear and angular velocity.

#### Minimap
This plugin provides several map-related functions, both in normal and VR mode. Provided a ROS OccupancyGrid message, it can display that map in the client in several ways.

* In an HTML element on the side (in normal mode)
* As a floor projection within the rendered scene (both modes)
* As a HUD element (in VR mode)

In addition, when used in combination with the [minimap ROS node][Minimap] on the robot, by clicking anywhere on the map, the location is sent as a goal to the ROS navigation stack.
Works seamlessly alongside the MouseDriver (e.g., if autonomous navigation is active, clicking on the image cancels the current goal, thus switching back to teleoperation).

#### Adaptive Autonomy
Provides functions related to switching and displaying of autonomy levels (teleoperation with assist, autonomous). The plugin communicates with and depends on the [assisted_teleop ROS node][Assisted Teleop].

Has indirect interactions with the Minimap and MouseDriver plugins.

#### Proximity Indicators
HUD elements indicating close proximity to nearby obstacles. Depends on the [assisted_teleop ROS node][Assisted Teleop] processed and binned laser data. 
Currently only works correctly with 360° laser scanners.


## Features

### [PLANNED] Proxy Mode

Proxy mode will allow a client to act as a proxy between other client and a robot. The main application of this is WoZ experiment setup.
Intended connection process:
1. The first client connects to the robot in a normal way. Assume client ID is C1 and robot ID is R.
2. Another client (C2) connect to C1.
3. C1 routes own video from C2 to R and video from R to C2.
4. C1 receives ROS messages from C2.

### [PLANNED] Local Mode Protocol
Local mode is useful when connection has to be establish between client and a robot on teh same local network, without internet access.
For local mode, sp00tn1k-ui will spawn a peer server (no stun/turn needed).

[Robot]: https://bitbucket.org/sp00tn1k/robot
[UI]: https://bitbucket.org/sp00tn1k/ui
[Base]: https://bitbucket.org/sp00tn1k/base
[Hardware]: https://bitbucket.org/sp00tn1k/hardware
[Peer Server]: https://bitbucket.org/sp00tn1k/peer-server
[Minimap]: https://bitbucket.org/sp00tn1k/robot/src/master/minimap/
[Assisted Teleop]: https://bitbucket.org/sp00tn1k/robot/src/master/assisted_teleop/
