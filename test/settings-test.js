/**
 * sp00tn1k settings test
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2020, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.0
 * @license GPL-3.0-or-later
 */

import * as assert from 'assert';

import { Settings } from '../src/js/settings.js';

describe('The Settings object', function () {
  it('should have a defined server description', function () {
    assert.notEqual('undefined', Settings.Server.Peer.Address);
  });
});
