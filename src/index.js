/**
 * sp00tn1k client software
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2020, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.0
 * @license GPL-3.0-or-later
 */

// Import Bootstrap and SCSS
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

// Own styles in SASS
import './scss/main.scss';

// Font-awesome for icons
import '@fortawesome/fontawesome-free/css/all.css';

// Regenerator runtime for async functions
import 'regenerator-runtime/runtime';

// Finally, load the core client class
import { Client } from './js/client.js';

/* When everything is loaded */
window.addEventListener('load', function () {
  // eslint-disable-next-line no-var, no-unused-vars
  var client = new Client();
});
