/* eslint-disable no-inline-comments */
import Peer from 'peerjs';
import { Settings } from './settings.js';

import { ROS } from './ros.js';

import { Plugin } from './plugins/plugin.js';
import { Battery } from './plugins/battery/battery.js';
import { MouseDriver } from './plugins/mouse_driver/mouse_driver.js';
import { Minimap } from './plugins/minimap/minimap.js';
import { AdaptiveAutonomy } from './plugins/adaptive_autonomy/adaptive_autonomy.js';
import { Velocity } from './plugins/velocity/velocity.js';
import { UVDisinfection } from './plugins/uv_disinfection/uv_disinfection.js';
import { ProximityIndicators } from './plugins/proximity_indicators/proximity_indicators.js';
import { XrROS } from './plugins/xr_ros/xr_ros.js';


class Connector {
  _peer; // peer is my peer connection to the server
  _conn; // conn is my data connection to the robot
  _call; // call is my media connection to the robot

  _ui; // my ui instance gets assigned on instantiation
  _renderer;
  _ros; // my ros instance only gets assigned when connection is established

  _plugins; // array of all plugins

  _peerTimeout;

  constructor(ui, renderer) {
    this._ui = ui;
    this._renderer = renderer;

    this._plugins = [];
    this._pluginButtons = [];


    this.pluginFactory = {
      velocity: Velocity,
      mousedriver: MouseDriver,
      adaptiveautonomy: AdaptiveAutonomy,
      battery: Battery,
      uvdisinfection: UVDisinfection,
      minimap: Minimap,
      proximityindicators: ProximityIndicators,
      xrros: XrROS
    };

    (async () => {
      await fetch('plugin.html').then(response => {
        return response.text();
      }).then(template => {
        this.pluginListTemplate = template;
        for (const plugin in this.pluginFactory) {
          const pluginText = Plugin.fill(this.pluginListTemplate, { plugin_name: plugin });
          this._ui.sidebarRight.firstElementChild.insertAdjacentHTML('beforeend', pluginText);
          this._pluginButtons[plugin] = document.getElementById(plugin + '_plugin_button');
          this._pluginButtons[plugin].disabled = true;
          this._pluginButtons[plugin].onclick = () => {
            this.addPlugin(plugin);
          };
        }
      }).catch(err => {
        this._ui.warn('[Could not load plugin list]: ' + err);
      });
    })();

    // user=rtc:o2i4yt28945tn
    // TODO: User management
    this.createPeer();

    /**
     * PeerJS connection management.
     * https://peerjs.com/docs.html#peeron
     */
    this._peer.on('open', id => {
      this._ui.log('My peer ID is: ' + id);
      this._ui.myPeerID.value = id;
      this._renderer.infoScreenMsg('Connected to peer server');
    });

    this._peer.on('close', () => {
      // this should not happen
      this._ui.warn('Closed peer connection, reloading in 30s...');
      if (this._ros) { this._ros.close(); }
      if (this._call) { this._call.close(); }
      if (this._conn) { this._conn.close(); }
      this._renderer.infoScreenMsg('Unable to connect to server');
      this._peer.destroy();
      this._peerTimeout = window.setTimeout(() => { this.createPeer(); }, 30 * 1000);
    });

    this._peer.on('disconnected', () => {
      if (!this._peer.destroyed) {
        this._ui.warn('Peer connection to server lost, attempting to reconnect in 30s.');
        this._peerTimeout = window.setTimeout(() => {
          this._peer.reconnect();
          if (!this._peer.disconnected) {
            this._ui.warn('Peer connection to server restored.');
          }
        }, 30 * 1000);
      }
    });

    this._peer.on('error', err => {
      this._ui.error('Peer error!');
      this._ui.error(err);
    });

    // use best possible video resolution
    const constraints = {
      audio: true,
      video: {
        optional: [
          { minWidth: 320 },
          { minWidth: 640 },
          { minWidth: 800 },
          { minWidth: 900 },
          { minWidth: 1024 },
          { minWidth: 1280 },
          { minWidth: 1920 },
          { minWidth: 2560 },
          { minWidth: 3840 }
        ]
      }
    };

    navigator.mediaDevices.getUserMedia(constraints)
      .then(mediaStream => {
        // my own stream
        this._ui.myVideo.srcObject = mediaStream;
        this._ui.myVideo.onloadedmetadata = () => {
          this._ui.myVideo.play();
        };
      })
      .catch(err => {
        this._ui.log(err.name + ': ' + err.message);
      }); // always check for errors at the end.
  }

  createPeer() {
    this._peer = new Peer('', {
      host: Settings.Server.Peer.Address,
      port: Settings.Server.Peer.Port,
      path: Settings.Server.Peer.Path,
      secure: true,
      debug: 2,
      config: {
        iceServers: [
          {
            urls: ['stun:' + Settings.Server.Stun.URL + ':' + Settings.Server.Stun.Port]
          }
        ]
      }
    });
  }

  connect() {
    if (this._ui.myVideo.srcObject == null) {
      this._ui.warn('No media stream provided. Cannot connect to robot.');
      this._renderer.infoScreenMsg('No media stream provided');
      return;
    }

    if (!this._peer.open) {
      this._ui.warn('Not connected to the peer server. Cannot connect to robot.');
      this._renderer.infoScreenMsg('Not connected to peer server');
      return;
    }

    // get robot peer id
    const robotID = this._ui.robotPeerID.value;

    // start data connection to the robot
    this._ui.log('Connecting to ' + robotID);

    this._conn = this._peer.connect(robotID);

    this._conn.on('open', () => {
      // established data connector
      this._ui.log('Connected to ' + robotID);
      this._renderer.infoScreenMsg('Connected to ' + robotID);
      this._ui.robotPeerID.setAttribute('disabled', '');

      // change button look and behaviour
      this._ui.connectButton.classList.remove('btn-primary');
      this._ui.connectButton.classList.add('btn-danger');
      this._ui.connectButton.onclick = () => {
        this.disconnect();
      };
      // add an event listener to make a proper disconnect if window is closed or reloaded
      window.addEventListener('beforeunload', () => {
        if (this._ros) {
          this.disconnect();
        }
      }, false);

      // pass _conn to _ros, so that the latter can send data and use on data handler
      this._ros = new ROS(this._conn);

      this.configSubscriber = new ROS.Subscriber('client_settings', 'std_msgs/String', 1, (message) => {
        const msg = message.msg.data.replace(/'/g, '"');
        this._ui.robotConfig = JSON.parse(msg);
        console.log(this._ui.robotConfig);
      });
      this._ros.add(this.configSubscriber);

      // wait a moment to receive settings relevant for plugins and camera
      const msgTimeout = window.setTimeout(() => {
        if (this._ui.robotConfig?.camera?.type !== undefined && this._ui.robotConfig.camera.type === 'spherical') {
          this._renderer.createVideoSphere();
        } else {
          this._renderer.createVideoPlane();
        }

        this.addPlugin('mousedriver');
        this.addPlugin('adaptiveautonomy');
        this.addPlugin('minimap');
        this.addPlugin('proximityindicators');
        this.addPlugin('xrros');
      }, 2000);

      for (const button of Object.values(this._pluginButtons)) {
        button.disabled = false;
      }

      // start a video call
      this._call = this._peer.call(robotID, this._ui.myVideo.srcObject);

      // received a media stream from remote
      this._call.on('stream', remoteStream => {
        this._ui.log('Received media stream');
        // Show stream in some video/canvas element.
        this._ui.remoteVideo.srcObject = remoteStream;
        this._ui.remoteVideo.onloadedmetadata = () => {
          this._ui.remoteVideo.play();
        };
      });
      this._call.on('close', () => {
        this._ui.log('Remote media connection closed');
      });
      this._call.on('error', err => {
        // when media connection is lost, _ros and _data channels need to be also cut
        // this is important so that the client does not send any messages when video is lost
        this._ui.error('Lost remote media connection');
        this._ui.error(err);
        this.disconnect();
      });
    });

    this._conn.on('close', () => {
      this._ui.log('Remote data connection closed');
      this._ui.robotPeerID.removeAttribute('disabled');
    });
    this._conn.on('error', err => {
      this._ui.error('Lost remote data connection');
      this._ui.error(err);
      this.disconnect();
      this._peerTimeout = window.setTimeout(this.connect, 10 * 1000);
    });
  }

  addPlugin(plugin) {
    if (!this._plugins[plugin]) {
      this._plugins[plugin] = new this.pluginFactory[plugin](this._ui, this._ros, this._renderer);
      this._pluginButtons[plugin].classList.remove('btn-secondary');
      this._pluginButtons[plugin].classList.add('btn-primary');
      this._pluginButtons[plugin].onclick = () => {
        this.removePlugin(plugin);
      };
    }
  }

  removePlugin(plugin) {
    if (this._plugins[plugin]) {
      this._plugins[plugin].destroy();
      delete this._plugins[plugin];
      this._pluginButtons[plugin].classList.remove('btn-primary');
      this._pluginButtons[plugin].classList.add('btn-secondary');
      this._pluginButtons[plugin].onclick = () => {
        this.addPlugin(plugin);
      };
    }
  }

  /**
   * Update needs to be called periodically to let the connector do its job
   * @returns {undefined}
   */
  update() {
    if (typeof (this._ros) !== 'undefined') {
      this._ros.spin();
    }
    for (const plugin of Object.values(this._plugins)) {
      plugin.update();
    }
  }

  // funtion to disconnect
  disconnect() {
    this._ui.info('Disconnecting...');

    for (const p in this._plugins) {
      this.removePlugin(p);
    }
    this._renderer.camera.lookAt(0, 0, -1);
    this._renderer.destroyVideoCanvas();

    if (this._ros) {
      this._ros.close();
      delete this.configSubscriber;

      for (const button of Object.values(this._pluginButtons)) {
        button.disabled = true;
      }
    }
    // let tracks = this._ui.myVideo.srcObject.getTracks();
    // tracks.forEach(function(track) { track.stop(); });
    // this._ui.myVideo.srcObject = null;
    this._call.close();
    this._conn.close();

    this._ui.info('Recovering UI');
    this._renderer.infoScreenMsg('Connection closed');

    // change button look and behaviour
    this._ui.connectButton.classList.remove('btn-danger');
    this._ui.connectButton.classList.add('btn-primary');
    this._ui.connectButton.onclick = () => {
      this.connect();
    };
  }
}

export { Connector };
