/**
 * renderer is responsible for three js overlay over remote video
 * @type type
 */

import { Settings } from './settings.js';

import * as THREE from 'three';

import { WEBGL } from 'three/examples/jsm/WebGL.js';
import { VRButton } from 'three/examples/jsm/webxr/VRButton.js';
import { XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory.js';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module.js';
import * as HelvFont from 'three/examples/fonts/helvetiker_regular.typeface.json';

class Renderer {
  scene;
  camera;
  floor;
  light;
  dome;
  renderer;
  _ui;
  msgTimeout;

  constructor(ui) {
    this._ui = ui;

    this._ui.log('Initializing renderer...');

    this.settings_gui = new GUI({ autoplace: false });
    this.settings_gui.domElement.id = 'controls_gui';
    this._ui.container.appendChild(this.settings_gui.domElement);

    // Create, position and add camera, light, floor
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(
      Settings.Camera.FOV,
      Settings.Renderer.AspectRatio,
      Settings.Camera.Near,
      Settings.Camera.Far
    );
    this.camera.layers.enable(0);
    this.camera.position.set(Settings.Camera.Position.x, Settings.Camera.Position.y, Settings.Camera.Position.z);

    this.scene.add(this.camera);

    this.light = new THREE.DirectionalLight(0xffffff);
    this.light.position.set(0, 0.5, 1).normalize();
    this.scene.add(this.light);

    const floorGeometry = new THREE.PlaneGeometry(200, 200, 10, 10);
    const floorCircleGeometry = new THREE.CircleGeometry(200, 32);
    const floorMaterial = new THREE.MeshBasicMaterial({
      color: 0xa0a0a0,
      side: THREE.DoubleSide,
      wireframe: true,
      opacity: 0.05,
      transparent: true
    });

    this.intersectables = []; // Objects among which the raycaster checks for intersections
    this.floorPivot = new THREE.Group();
    this.floorPivot.position.set(0, -40, 0);
    this.floor = new THREE.Mesh(floorGeometry, floorMaterial);
    this.floorPivot.add(this.floor);
    this.floor.position.y = 0.01;
    this.floor.rotation.x = Math.PI / 2;
    this.scene.add(this.floorPivot);


    // Map plane
    const mapGeometry = new THREE.PlaneGeometry(1, 1, 50, 50);
    const mapMaterial = new THREE.MeshBasicMaterial({
      color: 0xa0a0a0,
      side: THREE.DoubleSide,
      wireframe: true,
      opacity: 0.25,
      transparent: true
    });
    this.map = new THREE.Mesh(mapGeometry, mapMaterial);
    this.intersectables.push(this.map);
    this.map.layers.set(0);
    this.map.position.y = 0.0;
    this.map.rotation.x = Math.PI / 2;
    this.mapPivot = new THREE.Group();
    this.mapPivot.position.y = -5;
    this.scene.add(this.mapPivot);

    // Components for a screen message mesh
    this.screenMsgGroup = new THREE.Group();
    this.camera.add(this.screenMsgGroup);
    this.screenMsgGroup.position.set(0, 0.35, -1.6);
    this.screenMsgMaterial = new THREE.MeshBasicMaterial({ color: 0x345356 });


    this.hudFont = new THREE.Font(HelvFont);


    this._ui.remoteVideo.hidden = true;


    // Create renderer
    if (WEBGL.isWebGL2Available() === false) {
      // WebGL2 not available, fall back to WebGL1
      this.renderer = new THREE.WebGLRenderer({
        canvas: this._ui.canvas,
        alpha: Settings.Renderer.Alpha,
        antialias: Settings.Renderer.Antialiasing,
        precision: Settings.Renderer.Precision,
        powerPreference: Settings.Renderer.PowerPreference
      });
    } else {
      // WebGL2 available, use it
      const context = this._ui.canvas.getContext('webgl2', {
        alpha: Settings.Renderer.Alpha
      });
      this.renderer = new THREE.WebGLRenderer({
        canvas: this._ui.canvas,
        antialias: Settings.Renderer.Antialiasing,
        context: context
      });
      this._ui.log('Creating WEBGL 2 Renderer');
    }
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.visHeightAt1mDepth = 2 * Math.tan((this.camera.fov * Math.PI / 180) / 2);
    this.visWidthAt1mDepth = this.visHeightAt1mDepth * this.camera.aspect;

    this.tempMatrix = new THREE.Matrix4();
    this.tempVector = new THREE.Vector3();
    this.camera.xrOrientation = new THREE.Quaternion();
    this.camera.xrPosition = new THREE.Vector3();

    // Use right mouse button or two fingers on touchscreen to look around
	  const rotateStart = new THREE.Vector2();
	  const rotateEnd = new THREE.Vector2();
	  const rotateDelta = new THREE.Vector2();
	  this.spherical = new THREE.Spherical();
    this.camera.getWorldDirection(this.tempVector);
    this.spherical.setFromVector3(this.tempVector);
    this.rmb = false;

    this._ui.canvas.addEventListener('contextmenu', e => {
      e.preventDefault();
    });

    this._ui.canvas.addEventListener('mousedown', e => {
      if (e.which === 3) {
        this.rmb = true;
		    rotateStart.set(e.clientX, e.clientY);
      }
    });

    this._ui.canvas.addEventListener('touchstart', e => {
      if (e.touches.length == 2) {
        this.rmb = true;
		    rotateStart.set(e.targetTouches[0].clientX, e.targetTouches[0].clientY);
      }
    });

    this._ui.canvas.addEventListener('mousemove', e => {
      if (this.rmb) {
  		  rotateEnd.set(e.clientX, e.clientY);

  		  rotateDelta.subVectors(rotateEnd, rotateStart); // .multiplyScalar( scope.rotateSpeed );

		    this.spherical.theta -= (2 * Math.PI * rotateDelta.x / this._ui.canvas.height);

		    this.spherical.phi += (2 * Math.PI * rotateDelta.y / this._ui.canvas.height);
        this.spherical.phi = Math.max(Math.min(this.spherical.phi, Math.PI - 0.001), 0.001);

  		  rotateStart.copy(rotateEnd);
        this.camera.lookAt(this.tempVector.setFromSpherical(this.spherical));
      }
    }, false);

    this._ui.canvas.addEventListener('touchmove', e => {
      if (this.rmb) {
  		  rotateEnd.set(e.targetTouches[0].clientX, e.targetTouches[0].clientY);

  		  rotateDelta.subVectors(rotateEnd, rotateStart); // .multiplyScalar( rotateSpeed );

		    this.spherical.theta -= (2 * Math.PI * rotateDelta.x / this._ui.canvas.height);

		    this.spherical.phi += (2 * Math.PI * rotateDelta.y / this._ui.canvas.height);
        this.spherical.phi = Math.max(Math.min(this.spherical.phi, Math.PI - 0.001), 0.001);

  		  rotateStart.copy(rotateEnd);
        this.camera.lookAt(this.tempVector.setFromSpherical(this.spherical));
      }
    }, false);

    this._ui.canvas.addEventListener('mouseup', e => {
      if (e.which === 3) {
        this.rmb = false;
      }
    });

    this._ui.canvas.addEventListener('touchend', e => {
      this.rmb = false;
    });

    this._ui.canvas.addEventListener('mouseleave', e => {
      this.rmb = false;
    });


    // VR

    this.renderer.xr.enabled = true;
    this.renderer.xr.setReferenceSpaceType('local');
    const maxAnisotropy = this.renderer.capabilities.getMaxAnisotropy();
    const vrButton = VRButton.createButton(this.renderer);
    vrButton.id = 'vr_button';
    vrButton.style.zIndex = '1060';
    vrButton.style.bottom = '';
    this._ui.container.appendChild(vrButton);


    this.controller1 = this.renderer.xr.getController(0);
    this.controller1.addEventListener('connected', e => {
      this.controller1.gamepad = e.data.gamepad;
      this.infoScreenMsg('Controller connected');
    });
    this.controller1.addEventListener('selectstart', this.onSelectStart);
    this.controller1.addEventListener('selectend', (e) => { this.onSelectEnd(e); });

    this.controller2 = this.renderer.xr.getController(1);
    this.controller2.addEventListener('connected', e => {
      this.controller2.gamepad = e.data.gamepad;
      this.infoScreenMsg('Controller connected');
      // let pulse = this.controller2.gamepad.hapticActuators[0].pulse(0.8, 100);
    });
    this.controller2.addEventListener('selectstart', (e) => this.onSelectStart(e));
    this.controller2.addEventListener('selectend', (e) => { this.onSelectEnd(e); });

    this.controllerModelFactory = new XRControllerModelFactory();
    const lineGeometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, -1)]);
    const lineMaterial = new THREE.LineBasicMaterial({
      color: 0x777777,
      transparent: true,
      opacity: 0.4
    });

    this.controllerGrip1 = this.renderer.xr.getControllerGrip(0);
    this.controllerGrip1.add(this.controllerModelFactory.createControllerModel(this.controllerGrip1));
    this.scene.add(this.controllerGrip1);
    const controllerPointerLine1 = new THREE.Line(lineGeometry, lineMaterial.clone());
    controllerPointerLine1.scale.z = 1.0;
    this.controller1.add(controllerPointerLine1);
    this.controller1Raycaster = new THREE.Raycaster();
    this.controller1Intersects = [];
    const intersectDotGeometry = new THREE.SphereGeometry(0.02, 32, 16);
    const intersectDotMaterial = new THREE.MeshBasicMaterial({
      color: 0xff3333,
      side: THREE.DoubleSide,
      opacity: 0.0,
      transparent: true
    });
    this.controller1IntersectDot = new THREE.Mesh(intersectDotGeometry, intersectDotMaterial);
    this.scene.add(this.controller1IntersectDot);

    this.controllerGrip2 = this.renderer.xr.getControllerGrip(1);
    this.controllerGrip2.add(this.controllerModelFactory.createControllerModel(this.controllerGrip2));
    this.scene.add(this.controllerGrip2);
    lineMaterial.color.setHex(0xdd7777);
    const controllerPointerLine2 = new THREE.Line(lineGeometry, lineMaterial.clone());
    controllerPointerLine2.scale.z = 1.0;
    this.controller2.add(controllerPointerLine2);

    this.renderer.xr.addEventListener('sessionstart', e => {
      this.scene.add(this.controller1);
      this.scene.add(this.controller2);
    });

    this.renderer.xr.addEventListener('sessionend', e => {
      this.scene.remove(this.controller1);
      this.scene.remove(this.controller2);
      this.camera.lookAt(0, 0, -1);
      this.camera.getWorldDirection(this.tempVector);
      this.spherical.setFromVector3(this.tempVector);
    });

    window.setInterval(() => { // Update xr camera position and orientation
      if (this.renderer.xr.isPresenting) {
        this.camera.getWorldDirection(this.camera.xrOrientation);
        this.camera.xrOrientation.setFromRotationMatrix(this.camera.matrixWorld);
        this.camera.xrPosition.setFromMatrixPosition(this.camera.matrixWorld);
      }
    }, 125);

    window.setInterval(() => { // Run update on xr controller intersections
      if (this.renderer.xr.isPresenting) {
        this.tempMatrix.identity().extractRotation(this.controller1.matrixWorld);
        this.controller1Raycaster.ray.origin.setFromMatrixPosition(this.controller1.matrixWorld);
		    this.controller1Raycaster.ray.direction.set(0, 0, -1).applyMatrix4(this.tempMatrix);
        if (this.intersectables.length > 0) {
          this.controller1Intersects = this.controller1Raycaster.intersectObjects(this.intersectables, true);
          if (this.controller1Intersects.length > 0) {
            this.controller1IntersectDot.position.set(this.controller1Intersects[0].point.x, this.controller1Intersects[0].point.y, this.controller1Intersects[0].point.z);
            this.controller1IntersectDot.material.opacity = 0.8;
          } else {
            this.controller1IntersectDot.material.opacity = 0.0;
          }
        }
      }
    }, 50);

    // line indicating robot's orientation
    const lineFront = new THREE.Line(lineGeometry);
    lineFront.scale.z = 100;
    lineFront.position.set(0, -40, 0);
    lineFront.material.transparent = true;
    lineFront.material.opacity = 0.4;
    this.scene.add(lineFront);


    const pointGeometry = new THREE.BufferGeometry();
    // pointGeometry.setAttribute('position',
    const pointMaterial = new THREE.PointsMaterial({ size: 10, sizeAttenuation: true, color: 0xaa5555, transparent: true, alphaTest: 0.5 });
    this.point = new THREE.Points(pointGeometry, pointMaterial);
    this.point.position.set(0, 2, -3);
    this.scene.add(this.point);


    // this.settings_gui.add(lineFront.material, 'opacity', 0, 1).listen();
    this.settings_gui.close();
    this.printSceneGraph();
  }

  /* Display info message as mesh object in front of camera */
  infoScreenMsg(string) {
    if (this.screenMsgGroup.active) {
      clearTimeout(this.msgTimeout);
      this.screenMsg.geometry.dispose();
      this.screenMsgGroup.remove(this.screenMsg);
    }
    const screenMsgGeometry = new THREE.TextGeometry(string, {
      font: this.hudFont,
      size: 0.06,
      height: 0.005
    });
    screenMsgGeometry.computeBoundingBox();

    clearTimeout(this.msgTimeout);
    this.screenMsg = new THREE.Mesh(screenMsgGeometry, this.screenMsgMaterial);
    this.screenMsgGroup.add(this.screenMsg);
    this.screenMsg.position.set(-screenMsgGeometry.boundingBox.max.x / 2, 0, 0);
    this.screenMsgGroup.active = true;
    this.msgTimeout = window.setTimeout(() => {
      this.screenMsgGroup.remove(this.screenMsg);
      screenMsgGeometry.dispose();
      this.screenMsgGroup.active = false;
    }, 1500);
  }

  onSelectStart(e) {
    this.controller1.gamepad = e.data.gamepad;
  }

  onSelectEnd(e) {

  }

  createVideoPlane() {
    const videoPlaneGeometry = new THREE.PlaneGeometry(1, 1);
    const videoTexture = new THREE.VideoTexture(this._ui.remoteVideo);
    videoTexture.center.x = 0.5;
    videoTexture.center.y = 0.5;
    const videoPlaneMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff, map: videoTexture });
    this.videoPlane = new THREE.Mesh(videoPlaneGeometry, videoPlaneMaterial);
    this.videoPlane.scale.y = this.visHeightatDepth(2) * 1.02;
    this.videoPlane.scale.x = this.videoPlane.scale.y * this._ui.remoteVideo.videoWidth / this._ui.remoteVideo.videoHeight;
    this.videoPlanePivot = new THREE.Group();
    this.videoPlanePivot.add(this.videoPlane);

    this.videoPlane.position.set(0, 0, -2);
    if (this._ui.robotConfig?.camera?.rotation !== undefined) {
      this.camera.rotation.set(this._ui.robotConfig.camera.rotation[1], 0, this._ui.robotConfig.camera.rotation[2]); 
      this.videoPlanePivot.rotation.set(this._ui.robotConfig.camera.rotation[1], 0, this._ui.robotConfig.camera.rotation[2], 
                               this._ui.robotConfig.camera.rotation[0]);
    }
    this.scene.add(this.videoPlanePivot);
    this.camera.getWorldDirection(this.tempVector);
    this.spherical.setFromVector3(this.tempVector);
  }

  createVideoSphere() {
    // Create 3D sphere for 360 camera image projection
    // const videoSphereGeometry = new THREE.SphereGeometry(5000, 16, 8, 5*Math.PI/4, Math.PI/2, Math.PI/6, Math.PI*2/3); //Math.PI is opposite camera
    const videoSphereGeometry = new THREE.SphereGeometry(30, 256, 128);
    const videoTexture = new THREE.VideoTexture(this._ui.remoteVideo);

    // rotate texture if physical camera is rotated
    videoTexture.center.x = 0.5;
    videoTexture.center.y = 0.5;
    // dome_texture.rotation = Math.PI/2;

    const videoSphereMaterial = new THREE.MeshBasicMaterial({ side: THREE.BackSide, color: 0xffffff, map: videoTexture });
    this.videoSphere = new THREE.Mesh(videoSphereGeometry, videoSphereMaterial);
    this.videoSphere.layers.set(0);
    this.videoSphere.scale.x = -1; // Make sphere surface point inward
    if (this._ui.robotConfig?.camera?.rotation !== undefined) {
      // In case of a 360 camera, rotate video sphere instead of virtual camera
      this.videoSphere.rotation.set(this._ui.robotConfig.camera.rotation[1], this._ui.robotConfig.camera.rotation[2],
        this._ui.robotConfig.camera.rotation[0]);
    }
    // this.videoSphere.rotation.y = -Math.PI/2; // Correct for theta yaw rotation (looking to the side otherwise)
    // this.videoSphere.rotation.x = -Math.PI/18; // Theta is leaning (pitched) forward on the robot, need to correct for this
    this.videoSphere.position.set(0, 0, 0);
    this.scene.add(this.videoSphere);
  }

  destroyVideoCanvas() {
    if (this.videoSphere) {
      this.scene.remove(this.videoSphere);
      this.videoSphere.geometry.dispose();
      this.videoSphere.material.map.dispose();
      this.videoSphere.material.dispose();
      delete this.videoSphere;
    }
    if (this.videoPlane) {
      this.scene.remove(this.videoPlanePivot);
      this.videoPlane.geometry.dispose();
      this.videoPlane.material.map.dispose();
      this.videoPlane.material.dispose();
      delete this.videoPlanePivot;
      delete this.videoPlane;
    }
  }

  update() {
    this.renderer.setAnimationLoop(() => {
      this.renderer.render(this.scene, this.camera);
    });
  }

  setSize(width, height) {
    if (!this.renderer.xr.isPresenting) {
      this.renderer.setSize(width, height);
    }
    this.visWidthAt1mDepth = this.visHeightAt1mDepth * (this._ui.canvas.width / this._ui.canvas.height);
    this.camera.updateProjectionMatrix();
  }

  visHeightatDepth(depth) {
    return 2 * Math.tan((this.camera.fov * Math.PI / 180) / 2) * depth;
  }

  visWidthtatDepth(depth) {
    return this.visHeightatDepth(depth) * this.camera.aspect;
  }

  printSceneGraph() {
    this.scene.traverse(obj => {
      let s = '';
      let obj2 = obj;
      const worldposition = obj.getWorldPosition(new THREE.Vector3());
      const worlddirection = obj.getWorldDirection(new THREE.Vector3());
      while (obj2 != this.scene) {
        s = s + '|--';
        obj2 = obj2.parent;
      }
      console.log(s + obj.type + ' ' + JSON.stringify(obj.name) + ' ' + JSON.stringify(worldposition) + ' ' + JSON.stringify(worlddirection));
    });
  }
}

export { Renderer };
