/* eslint-disable no-console */
class ROS {
  _conn;
  // array of Subscribers
  _subscribers;
  // array of Publishers
  _publishers;
  // object of input and output message arrays
  _queue;

  /**
   * IMPORTNANT: Once provided, conn will be fully consumed by ROS.
   * This is because it is only possible to add one handler to the onmessage event.
   * @param {DataConnection} connector Instance of Peer's DataConnection
   */
  constructor(connector) {
    this._conn = connector;

    this._subscribers = [];
    this._publishers = [];

    this._queue = { input: [], output: [] };

    this._conn.on('data', data => {
      const message = JSON.parse(data);
      this._queue.input.push(message);
    });
  }

  /**
   * Close connection to ROS
   * @returns {undefined}
   */
  close() {
    console.info('Closing ROS connector');

    while (this._publishers.length > 0) {
      const p = this._publishers.shift();
      p.unadvertise();
    }
    while (this._subscribers.length > 0) {
      const s = this._subscribers.shift();
      s.unsubscribe();
    }

    this.spin();
  }

  // add a publisher or a subscriber
  add(thing) {
    if (thing instanceof ROS.Publisher) {
      thing._queue = this._queue;
      this._publishers.push(thing);
      console.info('Adding publisher for ' + thing.topic);
    } else if (thing instanceof ROS.Subscriber) {
      thing._queue = this._queue;
      this._subscribers.push(thing);
      console.info('Adding subscriber for ' + thing.topic);
    } else {
      console.error('Undefined type.');
    }
  }

  remove(thing) {
    if (thing instanceof ROS.Publisher) {
      for (let p = 0; p < this._publishers.length; p++) {
        if (this._publishers[p] === thing) {
          this._publishers[p].unadvertise();
          this._publishers.splice(p, 1);
        }
      }
    } else if (thing instanceof ROS.Subscriber) {
      for (let s = 0; s < this._subscribers.length; s++) {
        if (this._subscribers[s] === thing) {
          this._subscribers[s].unsubscribe();
          this._subscribers.splice(s, 1);
        }
      }
    }
  }

  spin() {
    this._publishers.forEach(p => {
      if (p._initialized !== true) {
        p.advertise();
        p._initialized = true;
      }
    });

    this._subscribers.forEach(s => {
      if (s._initialized !== true) {
        s.subscribe();
        s._initialized = true;
      }
    });

    // call handlers for received messages
    while (this._queue.input.length > 0) {
      const msg = this._queue.input.shift();
      this._subscribers.forEach(s => {
        if (s.topic === msg.topic) {
          s.handler(msg);
        }
      });
    }

    // TODO: Also check media connection status?
    if (this._conn.open) {
      // send all messages
      // todo: safe exit
      while (this._queue.output.length > 0) {
        const msg = this._queue.output.shift();
        this._conn.send(JSON.stringify(msg));
      }
    }
  }
}

ROS.Publisher = class {
  constructor(topic, type) {
    this._queue = null;
    this._initialized = false;

    this.topic = topic;
    this.type = type;
  }

  advertise() {
    if (this._queue !== null) {
      const message = {
        op: 'advertise',
        topic: this.topic,
        type: this.type
      };
      this._queue.output.push((message));
    }
  }

  unadvertise() {
    if (this._queue !== null) {
      const message = {
        op: 'unadvertise',
        topic: this.topic
      };
      this._queue.output.push((message));
    }
  }

  publish(msg) {
    if (this._queue !== null) {
      const message = {
        op: 'publish',
        topic: this.topic,
        msg: msg
      };
      this._queue.output.push((message));
    }
  }
};

ROS.Subscriber = class {
  constructor(topic, type, rate, handler) {
    this._queue = null;
    this._initialized = false;

    this.handler = handler;
    this.topic = topic;
    this.type = type;
    this.rate = rate;
  }

  subscribe() {
    if (this._queue !== null) {
      const message = {
        op: 'subscribe',
        topic: this.topic,
        type: this.type,
        throttle_rate: this.rate
      };
      this._queue.output.push((message));
    }
  }

  unsubscribe() {
    if (this._queue !== null) {
      const message = {
        op: 'unsubscribe',
        topic: this.topic,
        type: this.type,
        throttle_rate: this.rate
      };
      this._queue.output.push((message));
    }
  }
};

export { ROS };
