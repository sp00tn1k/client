/**
 * This class contains helper function to handle screen resize. This is needed to adjust renderer and remote video parameters.
 */

class Resizer {
  static update(ui, renderer) {
    const style = window.getComputedStyle(ui.container, null);
    const width = parseFloat(style.getPropertyValue('width'));
    const height = parseFloat(style.getPropertyValue('height'));

    ui.remoteVideo.style.width = height + 'px';
    ui.remoteVideo.style.height = width + 'px';
    // ui.remoteVideo.style.rotate = -90deg;
    ui.remoteVideo.width = height;
    ui.remoteVideo.height = width;
    ui.remoteVideo.style.top = (height - width) / 2 + 'px';

    renderer.setSize(width, height);
    renderer.camera.aspect = width / height;
    renderer.camera.updateProjectionMatrix();

    ui.canvasRect = ui.canvas.getBoundingClientRect();
  }
}

export { Resizer };
