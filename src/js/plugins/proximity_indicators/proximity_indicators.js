/**
 * UI element and a corresponding ROS subscriber for displaying a 3D HUD element indicating nearby obstacles
 */

import * as THREE from 'three';
import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class ProximityIndicators extends Plugin {
  constructor(ui, ros, renderer) {
    super(ui, ros);

    this._renderer = renderer;

    const data = {
      prefix: Plugin.id(),
      // subscriber parameters
      proxTopic: '/sp00tn1k/proximities',
      proxType: 'std_msgs/Int8MultiArray',
      rate: 1.0
    };

    this.proxPivot = new THREE.Group();
    this.proxPivot.position.set(this._renderer.visWidthAt1mDepth / 2 * 0.7, -this._renderer.visHeightAt1mDepth / 2 * 0.7, -1.0); // Place group in lower right corner
    // this.proxPivot.rotation.set(-Math.PI/2, 0, 0);
    // this.proxPivot.rotation.set(-3*Math.PI/4, 0, 0);
    this._renderer.camera.add(this.proxPivot);
    this.indicators = []; // The actual objects
    this.ringPivots = []; // Pivot elements for rotation

    this.proxGeometry = new THREE.RingGeometry(0.03, 0.04, 4, 1, 0, Math.PI / 2.5);
    this.proxMaterial = new THREE.MeshBasicMaterial({ color: 0xed041e, side: THREE.DoubleSide, transparent: true });

    for (let i = 0; i < 8; i++) {
      this.ringPivots.push(new THREE.Group());
      this.proxPivot.add(this.ringPivots[i]);
      this.indicators.push(new THREE.Mesh(this.proxGeometry, this.proxMaterial.clone()));
      this.ringPivots[i].add(this.indicators[i]);
      this.indicators[i].position.set(0.04, 0.04, 0);
      this.ringPivots[i].rotation.set(0, 0, i * Math.PI / 4 + 3 * Math.PI / 8);
      this.indicators[i].material.opacity = 0.0;
    }

    this.subscriber = new ROS.Subscriber(data.proxTopic, data.proxType, data.rate, (msg) => {
      for (let i = 0; i < 8; i++) {
        if (msg.msg.data[i] == 0) {
          this.indicators[i].visible = false;
        } else if (msg.msg.data[i] == 1) {
          this.indicators[i].visible = true;
          this.indicators[i].material.color.setHex(0xed041e);
          this.indicators[i].material.opacity = 0.3;
        } else if (msg.msg.data[i] == 2) {
          this.indicators[i].visible = true;
          this.indicators[i].material.opacity = 0.6;
        } else if (msg.msg.data[i] == 3) {
          this.indicators[i].visible = true;
          this.indicators[i].material.color.setHex(0x343a40);
          this.indicators[i].material.opacity = 0.3;
        }
      }
    });
    this._ros.add(this.subscriber);

    let resizeTimeout;
    window.addEventListener('resize', this.windowResizeEL = () => {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(() => {
        if (!this._renderer.renderer.xr.isPresenting) {
          this.proxPivot.position.set(this._renderer.visWidthAt1mDepth / 2 * 0.7, -this._renderer.visHeightAt1mDepth / 2 * 0.7, -1.0);
        } else {
          this.proxPivot.position.set(this._renderer.visWidthAt1mDepth / 2 * 0.5, -this._renderer.visHeightAt1mDepth / 2 * 0.5, -1.0);
        }
      }, 250);
    });

    // VR
    // Move indicators towards the center when presenting
    this._renderer.renderer.xr.addEventListener('sessionstart', this.xrSessionStartEL = e => {
      this.proxPivot.position.set(this._renderer.visWidthAt1mDepth / 2 * 0.5, -this._renderer.visHeightAt1mDepth / 2 * 0.5, -1.0);
    });
    this._renderer.renderer.xr.addEventListener('sessionend', this.xrSessionEndEL = e => {
      this.proxPivot.position.set(this._renderer.visWidthAt1mDepth / 2 * 0.7, -this._renderer.visHeightAt1mDepth / 2 * 0.7, -1.0);
    });

    this._ui.log('Proximity indicators plugin loaded');
  }

  update() {
  }

  destroy() {
    window.removeEventListener('resize', this.windowResizeEL);
    this._renderer.renderer.xr.removeEventListener('sessionstart', this.xrSessionStartEL);
    this._renderer.renderer.xr.removeEventListener('sessionend', this.xrSessionEndEL);

    this._ros.remove(this.subscriber);
    delete this.subscriber;
    this.proxGeometry.dispose();
    this.proxMaterial.dispose();
    for (let i = 0; i < 8; i++) {
      this.ringPivots[i].remove(this.indicators[i]);
      this.proxPivot.remove(this.ringPivots[i]);
      this.indicators[i].material.dispose();
    }
    this._renderer.camera.remove(this.proxPivot);
    this.indicators = [];
    this.ringPivots = [];
  }
}

export { ProximityIndicators };
