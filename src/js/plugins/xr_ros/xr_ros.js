/**
 * Plugin for publishing XR camera orientation on ROS
 *
 */

import * as THREE from 'three';
import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class XrROS extends Plugin {
  constructor(ui, ros, renderer) {
    super(ui, ros);

    this._renderer = renderer;

    this.xrCameraPosePublisher = new ROS.Publisher('xr_camera_pose', 'geometry_msgs/PoseStamped');
    this._ros.add(this.xrCameraPosePublisher);

    const poseHeader = {
      seq: 0,
      stamp: { sec: 0, nsec: 0 },
      frame_id: ''
    };

    window.setInterval(() => { // Update xr camera position and orientation
      if (this._renderer.renderer.xr.isPresenting && this._renderer.camera.xrPosition) {
        const d = Date.now();
        const s = Math.floor(d / 1000);
        const ns = d * 1000 - s * 1000000;
        poseHeader.seq++;
        poseHeader.stamp = { sec: s, nsec: ns };
        const pos = this._renderer.camera.xrPosition;
        const orient = this._renderer.camera.xrOrientation;

        this.xrCameraPosePublisher.publish({
          header: poseHeader,
          pose: {
            position: { x: -pos.z, y: -pos.x, z: pos.y },
            orientation: { x: -orient.z, y: -orient.x, z: orient.y, w: orient.w }
          }
        });
      }
    }, 125);

    this._ui.log('XR camera position plugin loaded!');
  }

  destroy() {
    this._ros.remove(this.xrCameraPosePublisher);
    delete this.xrCameraPosePublisher;
  }
}

export { XrROS };
