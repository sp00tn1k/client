/**
 * UI element and a corresponding ROS subscriber for current velocity.
 */

import * as THREE from 'three';
import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class Minimap extends Plugin {
  constructor(ui, ros, renderer) {
    super(ui, ros);

    this._renderer = renderer;

    const data = {
      prefix: Plugin.id(),
      // subscriber parameters
      mapTopic: '/sp00tn1k/minimap',
      mapType: 'nav_msgs/OccupancyGrid',
      mapRate: 1.0,

      // publsher parameters
      poseTopic: '/move_base_simple/goal',
      poseType: 'geometry_msgs/PoseStamped',
      initPoseTopic: '/initialpose',
      initPoseType: 'geometry_msgs/PoseWithCovarianceStamped',
      covariance: [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853892326654787],
      amclTopic: '/sp00tn1k/amcl_pose',
      amclType: 'geometry_msgs/PoseWithCovarianceStamped'
    };

    this.tempMatrix = new THREE.Matrix4();
    this.tempVector3 = new THREE.Vector3();
    this.tempVector2 = new THREE.Vector2();
    this.raycaster = new THREE.Raycaster();
    this.mapScalar = 2.4;

    // Array holding map markers
    this.mapMarkers = [];
    this.mapMarkers.dir = 'up';
    this.mapMarkers.posY = 0.25;
    this.markerGroup = new THREE.Group();
    this._renderer.mapPivot.add(this.markerGroup);

    // insert UI elements into the right sidebar panel
    (async () => {
      await fetch('minimap/minimap.html').then((response) => {
        return response.text();
      }).then((template) => {
        const tmpl = Plugin.fill(template, data);

        this._ui.sidebarRight.insertAdjacentHTML('beforeend', tmpl);

        this.canvas = document.getElementById(data.prefix + '_remoteMap_canvas');
        this.projectionSlider = document.getElementById(data.prefix + '_projection_opacity_slider');
        this.element = document.getElementById(data.prefix + '_container');

        this.projectionSlider.oninput = (e) => {
          this._renderer.map.material.opacity = e.target.value;
          this.markerMaterial.opacity = e.target.value;
          this.markerMaterial.needsUpdate = true;
        };


        // declare subscriber
        const mapTexture = new THREE.DataTexture();
        const canvasContext = this.canvas.getContext('2d');
        this._renderer.map.material.map = mapTexture;
        this.mapSubscriber = new ROS.Subscriber(data.mapTopic, data.mapType, data.mapRate, (message) => {
          // extract properties from minimap message
          this.canvas.width = message.msg.info.width;
          this.canvas.height = message.msg.info.height;
          this.mapOriginX = message.msg.info.origin.position.x;
          this.mapOriginY = message.msg.info.origin.position.y;
          this.originTheta = message.msg.info.origin.orientation.z;
          this.mapResolution = message.msg.info.resolution;
          this._renderer.map.width = (this.canvas.width * this.mapResolution) * this.mapScalar;
          this._renderer.map.height = (this.canvas.height * this.mapResolution) * this.mapScalar;

          const img = canvasContext.createImageData(message.msg.info.width, message.msg.info.height);

          for (let i = message.msg.info.height - 1; i > 0; i--) {
            for (let j = message.msg.info.width - 1; j > 0; j--) {
              img.data[message.msg.info.width * i * 4 + j * 4 + 0] = message.msg.data[message.msg.info.width * i * 3 + j * 3 + 0] * 4;
              img.data[message.msg.info.width * i * 4 + j * 4 + 1] = message.msg.data[message.msg.info.width * i * 3 + j * 3 + 1] * 4;
              img.data[message.msg.info.width * i * 4 + j * 4 + 2] = message.msg.data[message.msg.info.width * i * 3 + j * 3 + 2] * 4;
              img.data[message.msg.info.width * i * 4 + j * 4 + 3] = 256;
            }
          }

          canvasContext.putImageData(img, 0, 0); // translate context to center of canvas

          // project map on floor in renderer
          mapTexture.image = { data: img.data, width: message.msg.info.width, height: message.msg.info.height };
          mapTexture.needsUpdate = true;
          this._renderer.mapPivot.add(this._renderer.map);
          this._renderer.map.material.wireframe = false;
          this._renderer.map.material.depthTest = false;
          this._renderer.map.material.needsUpdate = true;

          // Create markers for PEIS locations. Temporarily hard coded, need to deliver as message
          if (this.mapMarkers.length == 0) {
            // this.createMapMarker('L', -0.2, 4.8);
            // this.createMapMarker('K', 2.4, 1.1);
            // this.createMapMarker('B', -1.9, 7.1);
            // this.createMapMarker('C', 0.2, 0.4);
            // this.createMapMarker('E', 1.5, 6.8);
          }
        });
        this._ros.add(this.mapSubscriber);


        this.amclSubscriber = new ROS.Subscriber(data.amclTopic, data.amclType, data.mapRate, (message) => {
          this.amclPose = message.msg.pose.pose.position;
          const amclOrientation = message.msg.pose.pose.orientation;

          // if a minimap is available, translate map projection to place robot approximately under camera
          if (!this._renderer.map.material.wireframe) {
            // resize map aspect ratio
            this._renderer.map.width = (this.canvas.width * this.mapResolution) * this.mapScalar;
            this._renderer.map.height = (this.canvas.height * this.mapResolution) * this.mapScalar;
            this._renderer.map.scale.set((this.canvas.width * this.mapResolution) * this.mapScalar,
              (this.canvas.height * this.mapResolution) * this.mapScalar, 1);

            this._renderer.map.position.x = (this.amclPose.x - this.mapOriginX) / (this.canvas.width * this.mapResolution) *
                                                                this._renderer.map.width - this._renderer.map.width / 2;
            this._renderer.map.position.z = -((this.amclPose.y - this.mapOriginY) / (this.canvas.height * this.mapResolution) *
                                                                this._renderer.map.height - this._renderer.map.height / 2);
            // rotate map to align with robot orientation
            const ang = new THREE.Euler().setFromQuaternion(new THREE.Quaternion(amclOrientation.x, amclOrientation.y, amclOrientation.z, amclOrientation.w));
            this._renderer.mapPivot.rotation.y = -ang.z - Math.PI / 2;
            this._renderer.mapPivot.position.z = 0.1;
            this.markerGroup.position.copy(this._renderer.map.position);
          }
        });
        this._ros.add(this.amclSubscriber);

        this.canvas.addEventListener('contextmenu', e => {
          e.preventDefault();
        });

        this._goalPublisher = new ROS.Publisher(data.poseTopic, data.poseType);
        this._ros.add(this._goalPublisher);
        this._initPosePublisher = new ROS.Publisher(data.initPoseTopic, data.initPoseType);
        this._ros.add(this._initPosePublisher);
        this._poseMsg = {
          position: { x: 0.0, y: 0.0, z: 0.0 },
          orientation: { x: 0.0, y: 0.0, z: 0.0, w: 1.0 }
        };
        this._initPoseMsg = {
          position: { x: 0.0, y: 0.0, z: 0.0 },
          orientation: { x: 0.0, y: 0.0, z: 0.0, w: 1.0 }
        };
        this._goalHeader = {
          seq: 0,
          stamp: { sec: 0, nsec: 0 },
          frame_id: 'map'
        };
        this._poseHeader = {
          seq: 0,
          stamp: { sec: 0, nsec: 0 },
          frame_id: 'map'
        };
        this._poseInWindow = {
          position: { x: 0.0, y: 0.0, z: 0.0 },
          orientation: { x: 0.0, y: 0.0, z: 0.0, w: 0.0 }
        };
        this._lmb = false;
        this._rmb = false;

        this.canvas.addEventListener('mousedown', this.canvasMouseDownEL = e => {
          const style = window.getComputedStyle(this.canvas);
          const width = this.mapOriginX + (this.canvas.width * this.mapResolution - e.offsetX * this.mapResolution * this.canvas.width /
                                  parseFloat(style.getPropertyValue('width')) - parseFloat(style.paddingLeft) - parseFloat(style.paddingRight));
          const height = this.mapOriginY + e.offsetY * this.mapResolution * this.canvas.height / parseFloat(style.getPropertyValue('height')) -
                                  parseFloat(style.paddingTop) - parseFloat(style.paddingBottom);
          if (e.which === 1) {
            this._poseMsg.position.x = width;
            this._poseMsg.position.y = height;
            this._poseInWindow.position.x = e.clientX;
            this._poseInWindow.position.y = e.clientY;
            this._lmb = true;
          }

          if (e.which === 3) {
            this._poseMsg.position.x = width;
            this._poseMsg.position.y = height;
            this._poseInWindow.position.x = e.clientX;
            this._poseInWindow.position.y = e.clientY;
            this._rmb = true;
          }
        });

        this.canvas.addEventListener('mouseup', this.canvasMouseUpEL = e => {
          const d = Date.now();
          const style = window.getComputedStyle(this.canvas);
          const width = -1 * (this.mapOriginX + e.offsetX * this.mapResolution * this.canvas.width / parseFloat(style.getPropertyValue('width')) -
                                                                              parseFloat(style.paddingLeft) - parseFloat(style.paddingRight));
          const height = this.mapOriginY + e.offsetY * this.mapResolution * this.canvas.height / parseFloat(style.getPropertyValue('height')) -
                                                                              parseFloat(style.paddingTop) - parseFloat(style.paddingBottom);

          const s = Math.floor(d / 1000);
          const ns = d * 1000 - s * 1000000;

          if (e.which === 1 && this._lmb) {
            var deltaX = width - this._poseMsg.position.x;
            var deltaY = height - this._poseMsg.position.y;
            var deltaWindowX = this._poseInWindow.position.x - e.clientX;
            var deltaWindowY = e.clientY - this._poseInWindow.position.y;
            var theta = Math.acos(deltaWindowX / Math.sqrt(Math.pow(deltaWindowX, 2) + Math.pow(deltaWindowY, 2))) * (deltaWindowY < 0 ? 1 : -1);
            if (Number.isNaN(theta)) { theta = 0; }
            var q = new THREE.Quaternion().setFromEuler(new THREE.Euler(0, 0, theta, 'XYZ'));
            this._poseMsg.orientation = { x: q.x, y: q.y, z: -q.z, w: q.w };

            this.publishNavGoal(this._poseMsg.position.x, this._poseMsg.position.y);
            this._lmb = false;
          }

          if (e.which === 3 && this._rmb) {
            var deltaX = width - this._poseMsg.position.x;
            var deltaY = height - this._poseMsg.position.y;
            var deltaWindowX = this._poseInWindow.position.x - e.clientX;
            var deltaWindowY = e.clientY - this._poseInWindow.position.y;
            var theta = Math.acos(deltaWindowX / Math.sqrt(Math.pow(deltaWindowX, 2) + Math.pow(deltaWindowY, 2))) * (deltaWindowY < 0 ? 1 : -1);
            if (Number.isNaN(theta)) { theta = 0; }
            var q = new THREE.Quaternion().setFromEuler(new THREE.Euler(0, 0, theta, 'XYZ'));

            this._poseMsg.orientation = { x: q.x, y: q.y, z: -q.z, w: q.w };

            this._poseHeader.seq++;
            this._poseHeader.stamp = { sec: s, nsec: ns };

            const pose = {
              pose: {
                position: this._poseMsg.position,
                orientation: this._poseMsg.orientation
              },
              covariance: data.covariance
            };
            const header = this._poseHeader;
            this._initPosePublisher.publish({ header, pose });
            this._rmb = false;
          }
        });

        // Use middle mouse button to switch between normal driving and autonomous mode with map projection
        this._ui.canvas.addEventListener('mousedown', this.uiCanvasMouseDownEL = e => {
          if (e.which === 2) {
            if (this._ui.navigationParams.mapMode) {
              this._ui.navigationParams.mapMode = false;

              this._renderer.infoScreenMsg('Map mode off');
            } else {
              this._ui.navigationParams.mapMode = true;
              this._renderer.infoScreenMsg('Map mode on');
              if (this.projectionSlider.value < 0.5) {
                this.projectionSlider.value = 0.5;
              }
            }
          }
        });

        this._ui.canvas.addEventListener('keydown', this.uiCanvasKeyDownEL = e => {
          if (e.keyCode === 77) { // key m
            if (this._ui.navigationParams.mapMode) {
              this._ui.navigationParams.mapMode = false;

              this._renderer.infoScreenMsg('Map mode off');
            } else {
              this._ui.navigationParams.mapMode = true;
              this._renderer.infoScreenMsg('Map mode on');
              if (this.projectionSlider.value < 0.5) {
                this.projectionSlider.value = 0.5;
              }
            }
          }
        });

        this._ui.canvas.addEventListener('mouseup', this.uiCanvasMouseUpEL = e => {
          if (this._ui.navigationParams.mapMode) {
            if (e.which === 1) {
              this.mouseVector.x = 2 * (e.clientX - this._ui.canvasRect.left) / this._ui.canvasRect.width - 1;
              this.mouseVector.y = -2 * (e.clientY - this._ui.canvasRect.top) / this._ui.canvasRect.height + 1;

              this.raycaster.setFromCamera(this.mouseVector, this._renderer.camera);
              const intersect = this.raycaster.intersectObject(this._renderer.map, true);
              if (intersect.length > 0) {
                this.worldToMap(intersect[0].point);
                this.publishNavGoal(this.tempVector2.x, this.tempVector2.y);
              }
            }
          }
        });


        // marker material
        this.markerMaterial = new THREE.MeshPhongMaterial({ color: 0xfaac1b, transparent: true });

        // Goal marker
        const goalMaterial = new THREE.MeshBasicMaterial({ color: 0xffcccc, transparent: true, opacity: 0.9 });
        this.goalSphere = new THREE.Mesh(new THREE.SphereGeometry(1, 8, 8), goalMaterial);
        this.goalPivot = new THREE.Group();
        this.markerGroup.add(this.goalPivot);
        this.goalPivot.add(this.goalSphere);
        this.goalPivot.position.copy(this.mapToWorld(-5, -5));

        this.tempVector3 = new THREE.Vector3();
        this.mouseVector = new THREE.Vector2();


        // VR
        const mapButtonGeometry = new THREE.CircleGeometry(0.1, 32);
        const mapButtonMaterial = new THREE.MeshBasicMaterial({
          color: 0x222222,
          transparent: true,
          opacity: 0.6
        });
        this.mapButton = new THREE.Mesh(mapButtonGeometry, mapButtonMaterial);

        this.mapButton.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.62, 0.3, -2);
        this.mapButton.active = false;
        this._renderer.intersectables.push(this.mapButton);

        const mapButtonRingGeometry = new THREE.RingGeometry(0.1, 0.11, 32);
        const mapButtonRingMaterial = new THREE.MeshBasicMaterial({
          color: 0xffffff,
          transparent: true,
          opacity: 0.6
        });
        this.mapButtonRing = new THREE.Mesh(mapButtonRingGeometry, mapButtonRingMaterial);
        this.mapButtonRing.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.62, 0.3, -2);

        this._renderer.renderer.xr.addEventListener('sessionstart', this.xrSessionStartEL = e => {
          this._renderer.camera.add(this.mapButton);
          this._renderer.camera.add(this.mapButtonRing);
        });

        this._renderer.renderer.xr.addEventListener('sessionend', this.xrSessionEndEL = e => {
          this._renderer.camera.remove(this.mapButton);
          this._renderer.camera.remove(this.mapButtonRing);
          if (this.mapButton.active) {
            this.mapButton.active = false;
            this.vrMapPlane.material.dispose();
            this.vrMapPlane.geometry.dispose();
            this._renderer.camera.remove(this.vrMapPlane);
          }
        });

        this._renderer.controller1.addEventListener('selectstart', this.xrCont1SelectStartEL = e => {
          // Check if the button was selected
          if (this._renderer.controller1Intersects.length > 0 && this._renderer.controller1Intersects[0].object == this.mapButton) {
            // var pulse = this._renderer.controller1.gamepad.hapticActuators[0].pulse(0.3, 100);
            if (this.mapButton.active) {
              this.mapButton.active = false;
              this._ui.navigationParams.mapMode = false;
              this.vrMapPlane.material.dispose();
              this.vrMapPlane.geometry.dispose();
              this._renderer.camera.remove(this.vrMapPlane);
              const index = this._renderer.intersectables.indexOf(this.vrMapPlane);
              index > -1 ? this._renderer.intersectables.splice(index, 1) : false;
            } else {
              this.mapButton.active = true;
              this._ui.navigationParams.mapMode = true;
              const vrMapGeometry = new THREE.PlaneGeometry(1, 1, 32);
              const vrMapMaterial = new THREE.MeshBasicMaterial({
                map: this._renderer.map.material.map,
                side: THREE.DoubleSide,
                transparent: true,
                opacity: 0.8
              });
              this.vrMapPlane = new THREE.Mesh(vrMapGeometry, vrMapMaterial);
              this.vrMapPlane.position.set(0, 0.2, -2.0);
              this._renderer.intersectables.push(this.vrMapPlane);

              // Scale map plane to map's aspect ratio (y is negative because texture is rotated on the axis)
              if (this._renderer.map.material.map.image.width > this._renderer.map.material.map.image.height) {
                this.vrMapPlane.scale.x = 1.5;
                this.vrMapPlane.scale.y = -1.5 * (this._renderer.map.material.map.image.height / this._renderer.map.material.map.image.width);
              } else {
                this.vrMapPlane.scale.x = 1.5 * (this._renderer.map.material.map.image.width / this._renderer.map.material.map.image.height);
                this.vrMapPlane.scale.y = -1.5;
              }
              this._renderer.camera.add(this.vrMapPlane);
              return;
            }
          }

          // Check if the map was selected
          if (this.mapButton.active) {
            if (this._renderer.controller1Intersects.length > 0 && this._renderer.controller1Intersects[0].object == this.vrMapPlane) {
              this.vrMapPlane.worldToLocal(this._renderer.controller1Intersects[0].point);
              const xm = (-this._renderer.controller1Intersects[0].point.x + 0.5) * this._renderer.map.width / this.mapScalar + this.mapOriginX;
              const ym = (this._renderer.controller1Intersects[0].point.y + 0.5) * this._renderer.map.height / this.mapScalar + this.mapOriginY;
              this.publishNavGoal(xm, ym);
            }
          } else if (this._renderer.controller1Intersects.length > 0 && this._renderer.controller1Intersects[0].object == this._renderer.map) {
            this.worldToMap(this._renderer.controller1Intersects[0].point);
            this.publishNavGoal(this.tempVector2.x, this.tempVector2.y);
          }
        });

        this._renderer.controller1.addEventListener('selectend', this.xrCont1SelectEndEL = e => {

        });

        if (this._renderer.renderer.xr.isPresenting) {
          this.xrSessionStartEL();
        }

        let resizeTimeout;
        window.addEventListener('resize', () => {
          clearTimeout(resizeTimeout);
          resizeTimeout = setTimeout(() => {
            this.mapButtonRing.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.62, 0, -2);
            this.mapButton.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.62, 0, -2);
          }, 100);
        });

        this._ui.log('Minimap plugin loaded');
      }).catch((err) => {
        this._ui.warn('[Minimap plugin]: ' + err);
      });
    })();
  }

  publishNavGoal(x, y) {
    this.d = performance.now();
    const s = Math.floor(this.d / 1000);
    const ns = this.d * 1000 - s * 1000000;
    this._goalHeader.seq++;
    this._goalHeader.stamp = { sec: s, nsec: ns };
    this._poseMsg.position.x = x;
    this._poseMsg.position.y = y;
    const header = this._goalHeader;
    const pose = this._poseMsg;
    this._goalPublisher.publish({ header, pose });
    this._ui.navigationParams.moveBaseActive = true;
    this._renderer.infoScreenMsg('New navigation goal sent');
  }

  // convert ROS map coordinates to world coordinates in the scene
  mapToWorld(mapPosX, mapPosY) {
    this.tempVector3.set(
      this._renderer.map.width / 2 - (-this.mapOriginX + mapPosX) * this.mapScalar,
      0,
      -this._renderer.map.height / 2 + (-this.mapOriginY + mapPosY) * this.mapScalar);
    return this.tempVector3;
  }

  // convert world coordinates to ROS map coordinates
  worldToMap(vector) {
    this._renderer.map.updateMatrixWorld();
    this._renderer.map.updateWorldMatrix(true, true);
    this._renderer.map.worldToLocal(vector);
    this.tempVector2.set(
      (-vector.x + 0.5) * this._renderer.map.width / this.mapScalar + this.mapOriginX,
      (vector.y + 0.5) * this._renderer.map.height / this.mapScalar + this.mapOriginY);
    return this.tempVector2;
  }

  createMapMarker(sign, mapPosX, mapPosY) {
    const markerGeometry = new THREE.TextGeometry(sign, {
      font: this._renderer.hudFont,
      size: 1,
      height: 0.05,
      bevelThickness: 0.01
    });
    markerGeometry.computeBoundingBox();
    const markerPivot = new THREE.Group();
    const marker = new THREE.Mesh(markerGeometry, this.markerMaterial);
    this.markerGroup.add(markerPivot);
    markerPivot.add(marker);
    markerPivot.position.copy(this.mapToWorld(mapPosX, mapPosY));
    marker.position.set(-markerGeometry.boundingBox.max.x / 2, 0, 0);
    this.mapMarkers.push(markerPivot);
  }

  update() {
    for (let i = 0; i < this.mapMarkers.length; i++) {
      this.mapMarkers[i].rotation.y += 0.02;
      this.mapMarkers[i].position.y = this.mapMarkers.posY;
    }
    if (this.mapMarkers.dir == 'up') {
      if (this.mapMarkers.posY < 0.5) {
        this.mapMarkers.posY += 0.005;
      } else { this.mapMarkers.dir = 'down'; }
    } else if (this.mapMarkers.posY > 0) {
      this.mapMarkers.posY -= 0.005;
    } else { this.mapMarkers.dir = 'up'; }
  }

  destroy() {
    if (this._renderer.renderer.xr.isPresenting) {
      this.xrSessionEndEL();
    }

    this._ui.canvas.removeEventListener('mousedown', this.uiCanvasMouseDownEL);
    this._ui.canvas.removeEventListener('mouseup', this.uiCanvasMouseUpEL);
    this._ui.canvas.removeEventListener('keydown', this.uiCanvasKeyDownEL);
    this.canvas.removeEventListener('mousedown', this.canvasMouseDownEL);
    this.canvas.removeEventListener('mouseup', this.canvasMouseUpEL);
    this._renderer.renderer.xr.removeEventListener('sessionstart', this.xrSessionStartEL);
    this._renderer.renderer.xr.removeEventListener('sessionend', this.xrSessionEndEL);
    this._renderer.controller1.removeEventListener('selectstart', this.xrCont1SelectStartEL);
    this._renderer.controller1.removeEventListener('selectend', this.xrCont1SelectEndEL);

    this._ros.remove(this.mapSubscriber);
    delete this.mapSubscriber;
    this._ros.remove(this.amclSubscriber);
    delete this.amclSubscriber;
    this._ros.remove(this._goalPublisher);
    delete this._goalPublisher;
    this._ros.remove(this._initPosePublisher);
    delete this._initPosePublisher;
    this.element.parentNode.removeChild(this.element);


    if (this._renderer.map.material.map) {
      this._renderer.map.material.map.dispose();
    }

    this._renderer.map.material.dispose();
    if (this.mapButton.active) {
      this.vrMapPlane.material.dispose();
      this.vrMapPlane.geometry.dispose();
      this._renderer.camera.remove(this.vrMapPlane);
    }
    this.goalSphere.material.dispose();
    this.mapButton.geometry.dispose();
    this.mapButton.material.dispose();
    this.mapButtonRing.geometry.dispose();
    this.mapButtonRing.material.dispose();

    while (this.mapMarkers.length > 0) {
      let marker = this.mapMarkers.shift();

      marker.children[0].geometry.dispose();
      marker.remove(marker.children[0]);
      this.markerGroup.remove(marker);
      marker = null;
    }
    this._renderer.mapPivot.remove(this.markerGroup);
    this.markerMaterial.dispose();

    this._renderer.mapPivot.remove(this._renderer.map);

    const index = this._renderer.intersectables.indexOf(this.vrMapPlane);
    index > -1 ? this._renderer.intersectables.splice(index, 1) : false;
  }
}

export { Minimap };
