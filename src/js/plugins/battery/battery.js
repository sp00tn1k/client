/**
 * UI element and a corresponding ROS subscriber for battery level. Exports Battery.
 * HTML template code is loaded from the corresponding HTML file.
 */

import { Plugin } from '../plugin.js';
import { ROS } from '../../ros.js';

/**
 * All plugins have to inherit from Plugin class, because they are stored and called in an array.
 */
class Battery extends Plugin {
  constructor(ui, ros) {
    // Parent constructor has to be invoked before any call to *this*.
    super(ui, ros);

    // Data to be used in the html templte. This is where the fill function will look for the data.
    const data = {
      // Prefix is a good thing to have to avoid potential conflicts between HTML elements' ID tags.
      // Prefix will be appended to the ID tags.
      prefix: Plugin.id(),
      // Subscriber parameters (topic and message type should match those on the robot side).
      topic: '/sp00tn1k/battery',
      type: 'sensor_msgs/BatteryState',
      // Rate is being sent as a part of subscribe message, however it is not always respected.
      rate: 1.0
    };

    // Start actual construction logic by loading HTML code from the template file.
    (async () => {
      await fetch('battery/battery.html').then(response => {
        return response.text();
      }).then(template => {
        // Use fill function to replace {{placeholders}} with real data.
        // The corresponding * placeholder * field must exist in the * data * object.
        const tmpl = Plugin.fill(template, data);

        // Insert the processed template code before the end of the right sidebar and obtain a handler to the battery voltage field.
        this._ui.container.insertAdjacentHTML('beforeend', tmpl);
        this.voltage = document.getElementById(data.prefix + '_battery');
        this.icon = document.getElementById(data.prefix + '_battery_hud');

        // This handle is needed to remove the plugin markup when it get destroyed.
        this.element = document.getElementById(data.prefix + '_container');

        this.subscriber = new ROS.Subscriber(data.topic, data.type, data.rate, msg => {
          // The only thing to do in this plugin is to copy battery voltage from the received message into the voltage field in UI.
          this.voltage.value = Math.round(msg.msg.voltage) + '%';
          if (msg.msg.voltage < 50 && this.icon.classList.contains('fa-battery-full')) {
            this.icon.classList.replace('fa-battery-full', 'fa-battery-half');
          } else if (msg.msg.voltage >= 50 && this.icon.classList.contains('fa-battery-half')) {
            this.icon.classList.replace('fa-battery-half', 'fa-battery-full');
          }
        });
        // Declare subscriber to the battery data.
        this._ros.add(this.subscriber);

        this._ui.log('Battery created!');
      }).catch(err => {
        this._ui.warn('[Battery plugin]: ' + err);
      });
    })();
  }

  destroy() {
    this.subscriber.unsubscribe();
    this.element.parentNode.removeChild(this.element);
  }
}

export { Battery };
