/**
 * UI element for adaptive autonomy
 * Controls and visualizes current autonomy level
 * Works with HTML and rendered sliders
 * Subscribes to move_base and assisted_teleop output
 * Slider can be set to select desired assistance level
 *
 * @type type
 */
import * as THREE from 'three';
import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class AdaptiveAutonomy extends Plugin {
  constructor(ui, ros, renderer) {
    super(ui, ros);

    this._renderer = renderer;

    this._ui.navigationParams = {
      moveBaseActive: false,
      assistLevel: 0,
      assistActual: 0,
      mapMode: false
    };

    const data = {
      prefix: Plugin.id(),
      // subscriber parameters
      // for actual current assist level
      assistActualTopic: 'assist_actual',
      assistActualType: 'std_msgs/Int8',

      moveBaseResultTopic: 'move_base/result',
      moveBaseResultType: 'move_base_msgs/MoveBaseActionResult',

      moveBaseStatusTopic: 'move_base/status',
      moveBaseStatusType: 'actionlib_msgs/GoalStatusArray',
      // publisher parameters
      assistLevelTopic: 'assist_level',
      assistLevelType: 'std_msgs/Int8',
      rate: 1.0,

      // for publishing move_base cancel request
      moveBaseCancelTopic: 'move_base/cancel',
      moveBaseCancelType: 'actionlib_msgs/GoalID'
    };

    (async () => {
      await fetch('adaptive_autonomy/adaptive_autonomy.html').then(response => {
        return response.text();
      }).then(template => {
        const tmpl = Plugin.fill(template, data);

        this._ui.sidebarLeft.insertAdjacentHTML('beforeend', tmpl);
        this.element = document.getElementById(data.prefix + '_container');

        this.autButton0 = document.getElementById(data.prefix + '_aut_button_l0');
        this.autButton1 = document.getElementById(data.prefix + '_aut_button_l1');
        this.autButton2 = document.getElementById(data.prefix + '_aut_button_l2');
        this.autButton3 = document.getElementById(data.prefix + '_aut_button_l3');

        this.autButton0Label = document.getElementById(data.prefix + '_aut_button_l0_label');
        this.autButton1Label = document.getElementById(data.prefix + '_aut_button_l1_label');
        this.autButton2Label = document.getElementById(data.prefix + '_aut_button_l2_label');
        this.autButton3Label = document.getElementById(data.prefix + '_aut_button_l3_label');

        this.autButton0Label.style.backgroundColor = '#23bebc';
        this.autButton1Label.style.backgroundColor = '#343a40'; // dark a.k.a. gray-800
        this.autButton2Label.style.backgroundColor = '#343a40';
        this.autButton3Label.style.backgroundColor = '#343a40';

			  this.autButton0Label.style.border = '2px solid #23bebc';
			  this.autButton0Label.style.borderRadius = '4px';
			  this.autButton1Label.style.border = '2px solid #6c757d';
			  this.autButton1Label.style.borderRadius = '4px';
			  this.autButton2Label.style.border = '2px solid #6c757d';
			  this.autButton2Label.style.borderRadius = '4px';
			  this.autButton3Label.style.border = '2px solid #6c757d';
			  this.autButton3Label.style.borderRadius = '4px';

        this.assistLevelPublisher = new ROS.Publisher(data.assistLevelTopic, data.assistLevelType);
        this._ros.add(this.assistLevelPublisher);

        // Cancel move_base goal if there is manual teleop input
        this.moveBaseCancelPublisher = new ROS.Publisher(data.moveBaseCancelTopic, data.moveBaseCancelType);
        this._ros.add(this.moveBaseCancelPublisher);

        // Track the actual assist level, i.e., if assist is currently doing anything
        this._ui.navigationParams.assistActual = 0;
        this.assistActualSubscriber = new ROS.Subscriber(data.assistActualTopic, data.assistActualType, data.rate, msg => {
          if (this._ui.navigationParams.assistActual !== msg.msg.data) {
            this._ui.navigationParams.assistActual = msg.msg.data;
            this.updateButtons();
          }
        });
        this._ros.add(this.assistActualSubscriber);

        this.moveBaseResultSubscriber = new ROS.Subscriber(data.moveBaseResultTopic, data.moveBaseResultType, data.rate, msg => {
          // if (msg.msg.status.status === 3 || msg.msg.status.status === 4 || (msg.msg.status.status === 2 && msg.msg.status.text === '')) {
          //  this._ui.navigationParams.moveBaseActive = false;
          // }
          //  this.updateButtons();
        });
        this._ros.add(this.moveBaseResultSubscriber);

        // Track the status of the last move_base goal. If it is not active, update moveBaseActive accordingly
        this.moveBaseStatusSubscriber = new ROS.Subscriber(data.moveBaseStatusTopic, data.moveBaseStatusType, data.rate, msg => {
          if (msg.msg.status_list.length > 0) {
            const goalStatus = msg.msg.status_list[msg.msg.status_list.length - 1];
            if (goalStatus.status === 3 || goalStatus.status === 4 || (goalStatus.status === 2 && goalStatus.text === '')) {
              this._ui.navigationParams.moveBaseActive = false;
            } else if (goalStatus.status === 1) {
              this._ui.navigationParams.moveBaseActive = true;
            }

            this.updateButtons();
          }
        });
        this._ros.add(this.moveBaseStatusSubscriber);


        this.autButton0.onchange = () => {
          this.autButton0Label.style.backgroundColor = '#23bebc';
          this.autButton1Label.style.backgroundColor = '#343a40';
          this.autButton2Label.style.backgroundColor = '#343a40';
          this.autButton0Label.style.borderColor = '#23bebc';
          this.autButton1Label.style.borderColor = '#6c757d';
          this.autButton2Label.style.borderColor = '#6c757d';
          this.loaButtonRing.material.color.setHex(0x222222);
          this.assistLevelPublisher.publish({ data: 0 });
        };
        this.autButton1.onchange = () => {
          this.autButton1Label.style.borderColor = '#23bebc';
          this.autButton2Label.style.backgroundColor = '#343a40';
          this.autButton2Label.style.borderColor = '#6c757d';
          this.loaButtonRing.material.color.setHex(0x228888);
          this.assistLevelPublisher.publish({ data: 1 });
        };
        this.autButton2.onchange = () => {
          this.autButton1Label.style.borderColor = '#23bebc';
          this.autButton2Label.style.borderColor = '#23bebc';
          this.loaButtonRing.material.color.setHex(0x23bebc);
          this.assistLevelPublisher.publish({ data: 2 });
        };

        this._ui.canvas.addEventListener('mousedown', this.uiCanvasMouseDownEL = e => {
          if (this._ui.navigationParams.moveBaseActive && !this._ui.navigationParams.mapMode) {
            const cancelMsg = {};
            this._ui.navigationParams.moveBaseActive = false;
            this.updateButtons();
            this.moveBaseCancelPublisher.publish(cancelMsg);
          }
        });

        // VR
        const loaButtonGeometry = new THREE.CircleGeometry(0.1, 32);
        const loaButtonMaterial = new THREE.MeshBasicMaterial({
          color: 0x222222,
          transparent: true,
          opacity: 0.6
        });
        this.loaButton = new THREE.Mesh(loaButtonGeometry, loaButtonMaterial);
        this.loaButton.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.65, -0.3, -2);

        const loaButtonRingGeometry = new THREE.RingGeometry(0.1, 0.11, 32);
        const loaButtonRingMaterial = new THREE.MeshBasicMaterial({
          color: 0xffffff,
          transparent: true,
          opacity: 0.6
        });
        this.loaButtonRing = new THREE.Mesh(loaButtonRingGeometry, loaButtonRingMaterial);
        this.loaButtonRing.position.set(-this._renderer.visWidthAt1mDepth / 2 * 2 * 0.65, -0.3, -2);

        this.loaButton.active = false;
        this._renderer.renderer.xr.addEventListener('sessionstart', this.xrSessionStartEL = e => {
          this._renderer.camera.add(this.loaButton);
          this._renderer.camera.add(this.loaButtonRing);
          this._renderer.intersectables.push(this.loaButton);
        });
        this._renderer.renderer.xr.addEventListener('sessionend', this.xrSessionEndEL = e => {
          this._renderer.camera.remove(this.loaButton);
          this._renderer.camera.remove(this.loaButtonRing);
        });

        this._renderer.controller1.addEventListener('selectstart', this.xrCont1SelectStartEL = e => {
          // Check if the button was selected
          if (this._renderer.controller1Intersects.length > 0 && this._renderer.controller1Intersects[0].object == this.loaButton) {
            this.onVRButtonPressed();
          }
        });

        if (this._renderer.renderer.xr.isPresenting) {
          this.xrSessionStartEL();
        }

        this._ui.log('Adaptive autonomy plugin loaded!');
      }).catch(err => {
        this._ui.warn('[Adaptive autonomy plugin]: ' + err);
      });
    })();
  }

  updateButtons() {
    if (this._ui.navigationParams.moveBaseActive) {
      this.loaButton.material.color.setHex(0x61ba2b);
      this.autButton3Label.style.backgroundColor = '#61ba2b';
      this.autButton0Label.style.backgroundColor = '#343a40';
      this.autButton1Label.style.backgroundColor = '#343a40';
      this.autButton2Label.style.backgroundColor = '#343a40';
    } else {
      this.autButton3Label.style.backgroundColor = '#343a40';
      this.autButton1Label.style.backgroundColor = '#343a40';
      this.autButton2Label.style.backgroundColor = '#343a40';
      this.autButton0Label.style.backgroundColor = '#23bebc';
      this.loaButton.material.color.setHex(0x222222);
      if (this._ui.navigationParams.assistActual == 1) {
        this.autButton1Label.style.backgroundColor = '#23bebc';
        if (this._ui.navigationParams.assistActual == 2) {
          this.autButton2Label.style.backgroundColor = '#23bebc';
          this.loaButton.material.color.setHex(0x23bebc);
        } else {
          this.autButton2Label.style.backgroundColor = '#343a40';
          this.loaButton.material.color.setHex(0x228888);
        }
      }
    }
  }

  onVRButtonPressed() {
    if (this._ui.navigationParams.moveBaseActive) {
      const cancelMsg = {};
      this._ui.navigationParams.moveBaseActive = false;
      this.updateButtons();
      this.moveBaseCancelPublisher.publish(cancelMsg);
    } else {
      if (++this._ui.navigationParams.assistLevel > 2) {
        this._ui.navigationParams.assistLevel = 0;
      }
      switch (this._ui.navigationParams.assistLevel) {
      case 0:
        this.loaButtonRing.material.color.setHex(0x222222);
        this._renderer.infoScreenMsg('Assist Level 0');
        this._ui.navigationParams.assistLevel = 0;
        break;
      case 1:
        this.loaButtonRing.material.color.setHex(0x228888);
        this._renderer.infoScreenMsg('Assist Level 1');
        this._ui.navigationParams.assistLevel = 1;
        break;
      case 2:
        this.loaButtonRing.material.color.setHex(0x23bebc);
        this._renderer.infoScreenMsg('Assist Level 2');
        this._ui.navigationParams.assistLevel = 2;
        break;
      case 3:
        this.loaButton.material.color.setHex(0x61ba2b);
        break;
      }
    }
  }

  update() {

  }

  destroy() {
    this.moveBaseCancelPublisher.publish({}); // Cancel move_base goal in case it's active

    if (this._renderer.renderer.xr.isPresenting) {
      this.xrSessionEndEL();
    }

    this._ui.canvas.removeEventListener('mousedown', this.uiCanvasMouseDownEL);
    this._renderer.renderer.xr.removeEventListener('sessionstart', this.xrSessionStartEL);
    this._renderer.renderer.xr.removeEventListener('sessionend', this.xrSessionEndEL);
    this._renderer.controller1.removeEventListener('selectstart', this.xrCont1SelectStartEL);

    this._ros.remove(this.assistLevelPublisher);
    delete this.assistLevelPublisher;
    this._ros.remove(this.moveBaseCancelPublisher);
    delete this.moveBaseCancelPublisher;
    this._ros.remove(this.assistActualSubscriber);
    delete this.assistActualSubscriber;
    this._ros.remove(this.moveBaseStatusSubscriber);
    delete this.moveBaseStatusSubscriber;
    this._ros.remove(this.moveBaseResultSubscriber);
    delete this.moveBaseResultSubscriber;
    this.element.parentNode.removeChild(this.element);

    this.loaButton.geometry.dispose();
    this.loaButtonRing.geometry.dispose();
    this.loaButton.material.dispose();
    this.loaButtonRing.material.dispose();

    const index = this._renderer.intersectables.indexOf(this.loaButton);
    index > -1 ? this._renderer.intersectables.splice(index, 1) : false;
  }
}

export { AdaptiveAutonomy };
