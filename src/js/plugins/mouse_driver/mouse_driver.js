/**
 * Default driving plugin
 *
 * @type type
 */

import * as THREE from 'three';

import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class MouseDriver extends Plugin {
  // _ui and _ros are inherited from Plugin
  _renderer;
  _mouse;
  _publisher;
  _lmb;

  navigationDisk;
  curveObject;

  _publisherFreq;
  _now;

  constructor(ui, ros, renderer) {
    super(ui, ros);

    this._renderer = renderer;
    this.settings = this._renderer.settings_gui.addFolder('Mouse Driver');

    this._publisherFreq = 5; // in Hz
    this._now = performance.now();

    if (this._ui.robotConfig?.plugins?.mouseDriver?.linearVel !== undefined) {
      this.maxLinearVel = this._ui.robotConfig.plugins.mouseDriver.linearVel;
    } else {
      this.maxLinearVel = 0.6; // default
    }
    if (this._ui.robotConfig?.plugins?.mouseDriver?.angularVel !== undefined) {
      this.maxAngularVel = this._ui.robotConfig.plugins.mouseDriver.angularVel;
    } else {
      this.maxAngularVel = 0.6; // default
    }

    this.settings.add(this, 'maxLinearVel', 0.1, 1).listen();
    this.settings.add(this, 'maxAngularVel', 0.1, 1).listen();

    const data = {
      topic: 'cmd_vel_in',
      type: 'geometry_msgs/Twist',
      prefix: Plugin.id(),

      moveBaseCancelTopic: 'move_base/cancel',
      moveBaseCancelType: 'actionlib_msgs/GoalID'
    };

    fetch('mouse_driver/mouse_driver.html').then(response => {
      return response.text();
    }).then(template => {
      const tmpl = Plugin.fill(template, data);

      this._ui.sidebarRight.firstElementChild.insertAdjacentHTML('beforeend', tmpl);
      this.element = document.getElementById(data.prefix + '_container');

      // attach on change event to the field, to make the plugin to re-advertise it
      this.cmdVelTopicField = document.getElementById(data.prefix + '_cmd_velTopic');

      this._publisher = new ROS.Publisher(data.topic, data.type);
      this._ros.add(this._publisher);


      this.cmdVelTopicField.addEventListener('input', this.cmdVelEL = e => {
        data.topic = e.target.value;
        this._publisher.unadvertise();
        this._publisher.topic = data.topic;
        this._publisher.advertise();
      });
    }).catch(err => {
      this._ui.warn('[Mouse driving plugin]: ' + err);
    });


    this._mouse = new THREE.Vector2();
    this._lmb = false;


    // Attach mouse move function to canvas
    this._ui.canvas.addEventListener('mousemove', this.canvasMouseMoveEL = e => {
      this._mouse.x = 2 * (e.clientX - this._ui.canvasRect.left) / this._ui.canvasRect.width - 1;
      this._mouse.y = -2 * (e.clientY - this._ui.canvasRect.top) / this._ui.canvasRect.height + 1;

      const intersect = this.mouseRaycaster.intersectObject(this._renderer.floor, true);
      if (intersect.length > 0) {
        this.curve.v0.set(0, this._renderer.floorPivot.position.y, 0);
        this.curve.v1.set(0, this._renderer.floorPivot.position.y, -20.0);
        this.curve.v2.set(intersect[0].point.x * 0.8, this._renderer.floorPivot.position.y, intersect[0].point.z);
        this.curve.v3.set(intersect[0].point.x, this._renderer.floorPivot.position.y, intersect[0].point.z);

        this.curveObject.geometry.dispose();
        this.curveObject.geometry = new THREE.BufferGeometry().setFromPoints(this.curve.getPoints(25));

        this.navigationDisk.position.set(intersect[0].point.x, this._renderer.floorPivot.position.y, intersect[0].point.z);
      }
    }, false);

    this._ui.canvas.addEventListener('touchstart', this.canvasTouchStartEL = e => {
      this._mouse.x = 2 * (e.targetTouches[0].clientX - this._ui.canvasRect.left) / this._ui.canvasRect.width - 1;
      this._mouse.y = -2 * (e.targetTouches[0].clientY - this._ui.canvasRect.top) / this._ui.canvasRect.height + 1;
      this._lmb = true;
      this.curveObject.material.color.setHex(0x007bff);
      this.navigationDisk.material.color.setHex(0x007bff);
      e.preventDefault();
    }, false);

    this._ui.canvas.addEventListener('touchend', this.canvasTouchEndEL = e => {
      this._lmb = false;
      e.preventDefault();
      this.curveObject.material.color.setHex(0xc0c0c0);
      this.navigationDisk.material.color.setHex(0xc0c0c0);
    }, false);

    this._ui.canvas.addEventListener('touchmove', this.canvasTouchMoveEL = e => {
      this._mouse.x = 2 * (e.targetTouches[0].clientX - this._ui.canvasRect.left) / this._ui.canvasRect.width - 1;
      this._mouse.y = -2 * (e.targetTouches[0].clientY - this._ui.canvasRect.top) / this._ui.canvasRect.height + 1;
      e.preventDefault();
    }, false);

    this._ui.canvas.addEventListener('mousedown', this.canvasMouseDownEL = e => {
      // Left mouse button was pressed, set flag
      if (e.which === 1) {
        this._lmb = true;
        this.curveObject.material.color.setHex(0x007bff);
        this.navigationDisk.material.color.setHex(0x007bff);
      }
    });

    window.addEventListener('mouseup', this.windowMouseUpEL = e => {
      // Left mouse button was released, clear flag
      if (e.which === 1) {
        this._lmb = false;
        this.curveObject.material.color.setHex(0xc0c0c0);
        this.navigationDisk.material.color.setHex(0xc0c0c0);
      }
    });

    this._ui.canvas.addEventListener('mouseleave', this.canvasMouseLeaveEL = e => {
      if (!this._lmb) {
        this.navigationDisk.material.opacity = 0.0;
        this.curveObject.material.opacity = 0.0;
      }
    });

    this._ui.canvas.addEventListener('mouseenter', this.canvasMouseEnterEL = e => {
      this.navigationDisk.material.opacity = 0.7;
      this.curveObject.material.opacity = 0.7;
    });

    this.tempMatrix = new THREE.Matrix4();
    this.vrSelected = false;

    this._renderer.controller1.addEventListener('selectstart', this.xrCont1SelectStartEL = e => {
      this.vrSelected = true;
    });

    this._renderer.controller1.addEventListener('selectend', this.xrCont1SelectEndEL = e => {
      this.vrSelected = false;
    });

    const navigationDiskGeometry = new THREE.CylinderGeometry(3, 3, 0.6, 32);
    const navigationDiskMaterial = new THREE.MeshLambertMaterial({ color: 0xc0c0c0, opacity: 0.7, transparent: true });
    navigationDiskMaterial.depthTest = false;
    this.navigationDisk = new THREE.Mesh(navigationDiskGeometry, navigationDiskMaterial);
    this.navigationDisk.position.set(0, -40, 0);
    this._renderer.scene.add(this.navigationDisk);

    this.curve = new THREE.CubicBezierCurve3(
      new THREE.Vector3(0, 0, 0),
      new THREE.Vector3(0, 0, 0),
      new THREE.Vector3(0, 0, 0),
      new THREE.Vector3(0, 0, 0)
    );
    const points = this.curve.getPoints(25);
    const curveObjectGeometry = new THREE.BufferGeometry().setFromPoints(points);
    const curveObjectMaterial = new THREE.LineBasicMaterial({ color: 0x007bff, opacity: 0.7, transparent: true });
    curveObjectMaterial.depthTest = false;

    this.curveObject = new THREE.Line(curveObjectGeometry, curveObjectMaterial);
    this._renderer.scene.add(this.curveObject);

    this.mouseRaycaster = new THREE.Raycaster();


    // VR
    this._renderer.renderer.xr.addEventListener('sessionstart', this.xrSessionStartEL = e => {
      this._renderer.camera.remove(this.navigationDisk);
      this._renderer.scene.remove(this.curveObject);
    });
    this._renderer.renderer.xr.addEventListener('sessionend', this.xrSessionEndEL = e => {
      this._renderer.camera.add(this.navigationDisk);
      this._renderer.scene.add(this.curveObject);
    });

    if (this._renderer.renderer.xr.isPresenting) { // If plugin is loaded while in XR mode
      this.xrSessionStartEL();
    }

    this._ui.log('Mouse Driver plugin loaded');
  }

  update() {
    const now = performance.now();

    /* collide with the floor */
    this.mouseRaycaster.setFromCamera(this._mouse, this._renderer.camera);

    const twist = {
      linear: {
        x: 0.0, y: 0.0, z: 0.0
      },
      angular: {
        x: 0.0, y: 0.0, z: 0.0
      }
    };

    if (this._lmb) {
      const intersects = this.mouseRaycaster.intersectObjects([this._renderer.floor], true);
      if (intersects.length > 0) {
        // driving
        twist.linear.x = Math.min(this._mouse.y + 1, this.maxLinearVel);
        if (this._mouse.y < -0.92) { twist.linear.x = -0.12; }
        twist.angular.z = -this._mouse.x * this.maxAngularVel;
      }
    }

    if (this._renderer.renderer.xr.isPresenting) {
      // Use controller touchpad for teleoperation
      if (this._renderer.controller1.gamepad && this._renderer.controller1.gamepad.buttons.length >= 3 && this._renderer.controller1.gamepad.buttons[2].pressed) {
        twist.linear.x = Math.max(Math.min(this._renderer.controller1.gamepad.axes[1] * this.maxLinearVel, this.maxLinearVel), -0.1);
        twist.angular.z = -this._renderer.controller1.gamepad.axes[0] * this.maxAngularVel;
        // this._renderer.infoScreenMsg('Twist: ' + twist.linear.x.toFixed(2) + ' , ' + twist.angular.z.toFixed(2));
        // var pulse = this._renderer.controller1.gamepad.hapticActuators[0].pulse(0.8, 100);
      }
    }
    // limit publishing frequency
    if ((now - this._now) > (1000 / this._publisherFreq)) {
      if ((typeof (this._ros) !== 'undefined') && (typeof (this._publisher) !== 'undefined') && !this._ui.navigationParams.mapMode) {
        // VR active
        if (this.vrSelected) {
          // If we want to use teleop with the select button
        }
        this._publisher.publish(twist);
      }
      this._now = now;
    }
  }

  destroy() {
    this.cmdVelTopicField.removeEventListener('input', this.cmdVelEL);
    this._ui.canvas.removeEventListener('mousemove', this.canvasMouseMoveEL);
    this._ui.canvas.removeEventListener('touchstart', this.canvasTouchStartEL);
    this._ui.canvas.removeEventListener('touchend', this.canvasTouchEndEL);
    this._ui.canvas.removeEventListener('touchmove', this.canvasTouchMoveEL);
    this._ui.canvas.removeEventListener('mousedown', this.canvasMouseDownEL);
    window.removeEventListener('mouseup', this.windowMouseUpEL);
    this._ui.canvas.removeEventListener('mouseleave', this.canvasMouseLeaveEL);
    this._ui.canvas.removeEventListener('mouseenter', this.canvasMouseEnterEL);
    this._renderer.controller1.removeEventListener('selectstart', this.xrCont1SelectStartEL);
    this._renderer.controller1.removeEventListener('selectend', this.xrCont1SelectEndEL);
    this._renderer.renderer.xr.removeEventListener('sessionstart', this.xrSessionStartEL);
    this._renderer.renderer.xr.removeEventListener('sessionend', this.xrSessionEndEL);

    this._ros.remove(this._publisher);
    delete this._publisher;
    this.element.parentNode.removeChild(this.element);
    this._renderer.scene.remove(this.navigationDisk);
    this._renderer.scene.remove(this.curveObject);
    this.navigationDisk.geometry.dispose();
    this.curveObject.geometry.dispose();
    this.navigationDisk.material.dispose();
    this.curveObject.material.dispose();

    this._renderer.settings_gui.removeFolder(this.settings);
    delete this.settings;
  }
}

export { MouseDriver };
