/**
 * UI element and a corresponding ROS publisher for uv disinfection. Exports UVDisinfection.
 * If the button is active, continuously publishes 'true' on the uv_active topic. If inactive,
 * publishes 'false' once.
 * HTML template code is loaded from the corresponding HTML file.
 */

import { Plugin } from '../plugin.js';
import { ROS } from '../../ros.js';

/**
 * All plugins have to inherit from Plugin class, because they are stored and called in an array.
 */
class UVDisinfection extends Plugin {
  constructor(ui, ros, renderer) {
    // Parent constructor has to be invoked before any call to *this*.
    super(ui, ros);

    this._renderer = renderer;
    this._publisherFreq = 1;
    this._now = performance.now();

    // Data to be used in the html template. This is where the fill function will look for the data.
    const data = {
      // Prefix is a good thing to have to avoid potential conflicts between HTML elements' ID tags.
      // Prefix will be appended to the ID tags.
      prefix: Plugin.id(),
      // Publisher parameters (topic and message type should match those on the robot side).
      topic: '/sp00tn1k/uv_active',
      type: 'std_msgs/Bool'
    };

    // Start actual construction logic by loading HTML code from the template file.
    fetch('uv_disinfection/uv_disinfection.html').then(response => {
      return response.text();
    }).then(template => {
      // Use fill function to replace {{placeholders}} with real data.
      // The corresponding * placeholder * field must exist in the * data * object.
      const tmpl = Plugin.fill(template, data);

      this._ui.sidebarLeft.insertAdjacentHTML('beforeend', tmpl);

      // This handle is needed to remove the plugin markup when it get destroyed.
      this.element = document.getElementById(data.prefix + '_container');
      this._publisher = new ROS.Publisher(data.topic, data.type);
      this._publisher.lastMsg = performance.now();

      this.toggleButton = document.getElementById('buttonActivateUV');
      this.toggleButton.value = 'OFF';
      this.timer = document.getElementById(data.prefix + '_uv_elapsed');
      this.toggleButton.onclick = () => {
        this.toggle();
      };

      this.timer.addEventListener('input', this.timerInputEL = e => {
        this.timer.duration = e.target.value;
      });
      
      this.pir_active = false;
      this._pir_subscriber = new ROS.Subscriber('pir', 'std_msgs/Bool', 1.0, (msg) => {
        if (this.pir_active != msg.msg.data) {
          this.pir_active = msg.msg.data;
          this.toggle_pir();
        } 
      });
      // Declare publisher.
      this._ros.add(this._publisher);
      this._ros.add(this._pir_subscriber);
  
      this._ui.log('Disinfecion plugin created!');
    }).catch(err => {
      this._ui.warn('[Disinfection plugin]: ' + err);
    });
  }

  toggle() {
    if (this.toggleButton.value == 'OFF' && this.timer.value > 0) {
      this.toggleButton.value = 'ON';
      this.toggleButton.classList.remove('btn-primary');
      this.toggleButton.classList.add('btn-danger');
      this.timer.startTime = performance.now();
      this._renderer.infoScreenMsg('UV disinfection started');
      this.timer.readOnly = true;
    } else {
      this.toggleButton.value = 'OFF';
      this.toggleButton.classList.remove('btn-danger');
      this.toggleButton.classList.add('btn-primary');
      this._renderer.infoScreenMsg('UV disinfection completed');
      this.timer.value = 0;
      this.timer.duration = 0;
      this.timer.readOnly = false;
    }
  }

  toggle_pir() {
    if (this.pir_active) {
      this._renderer.infoScreenMsg('UV disinfection interrupted');
      this.timer.duration = this.timer.clock;
    }
    else {
      this._renderer.infoScreenMsg('UV disinfection resumed');
      this.timer.startTime = performance.now();
    }

  }

  update()
  {
    const bool_msg = { data: false };

    const now = performance.now();
    if ((typeof (this._ros) !== 'undefined') && (typeof (this._publisher) !== 'undefined')) {
      if (this.toggleButton.value == 'ON') {
        if (!this.pir_active) {
          this.timer.clock = this.timer.duration - (now - this.timer.startTime) / 1000; 
          this.timer.value = Math.ceil(this.timer.clock);
          if ((now - this.timer.startTime)/1000 < this.timer.duration) {
            bool_msg.data = true;
          } else {
            this.toggle();
            bool_msg.data = false;
          }
        } else {
          bool_msg.data = false;
        }
      }

      // limit publishing frequency
      if ((now - this._publisher.lastMsg) > (1000 / this._publisherFreq)) {
        this._publisher.publish(bool_msg);
        this._publisher.lastMsg = now;
      }
      this._now = now;
    }
  }

  destroy() {
    this.timer.removeEventListener('input', this.timerInputEL);

    this._ros.remove(this._publisher);
    delete this._publisher;
    this._ros.remove(this._pir_subscriber);
    delete this._pir_subscriber;
    this.element.parentNode.removeChild(this.element);
  }
}

export { UVDisinfection };
