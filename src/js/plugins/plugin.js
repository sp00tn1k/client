/**
 * Parent class for plugins. Not to be instantiated. All other plugins must inherit from this.
 *
 * @type type
 */

/**
 * My video stream is in UI.myVideo
 * Remove video stream is in UI.remoteVideo
 */

class Plugin {
  _ui;
  _ros;
  /**
   * ROS and UI must be supplied even if not used.
   * @param {UI} ui
   * @param {ROS} ros
   */
  constructor(ui, ros) {
    this._ui = ui;
    this._ros = ros;
  }

  /**
   * Update will be called on every plugin at fixed period of time. This is needed to allow some plugins to process their data.
   */
  update() {

  }

  /**
   * Destroy method should gracefully free all allocated resources and remove UI elements.
   */
  destroy() {

  }

  /**
   * Helper function to generate an ID number to use within the plugin to e.g. make unique field IDs in the UI.
   */
  static id(length = 8) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result = result + characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  // do simple mustache-like replace
  // inspired by https://gomakethings.com/a-vanilla-js-alternative-to-handlebarsjs-and-mustachejs/
  // does not support any nested vars!
  static fill(template, data) {
    return template.replace(/\{\{([^}]+)\}\}/g, function (match) {
      // remove the wrapping curly braces
      match = match.slice(2, -2);
      // replace placeholder with the data, or leave it as is
      if (!data[match]) {
        return '{{' + match + '}}';
      }
      return data[match];
    });
  }
}

export { Plugin };
