/**
 * UI element and a corresponding ROS subscriber for current velocity.
 *
 * @type type
 */
import { ROS } from '../../ros.js';
import { Plugin } from '../plugin.js';

class Velocity extends Plugin {
  constructor(ui, ros) {
    super(ui, ros);

    // data to be used in the html template and the rest of the code
    const data = {
      prefix: Plugin.id(),
      topic: '/sp00tn1k/velocity',
      type: 'geometry_msgs/Twist',
      rate: 10.0
    };
    (async () => {
      await fetch('velocity/velocity.html').then(response => {
        return response.text();
      }).then(template => {
        const tmpl = Plugin.fill(template, data);

        this._ui.sidebarRight.firstElementChild.insertAdjacentHTML('beforeend', tmpl);
        this.element = document.getElementById(data.prefix + '_container');

        this.velocityLin = document.getElementById(data.prefix + '_vel_lin');
        this.velocityAng = document.getElementById(data.prefix + '_vel_ang');

        // declare subscriber
        this._subscriber = new ROS.Subscriber(data.topic, data.type, data.rate, msg => {
          this.velocityLin.value = msg.msg.linear.x.toPrecision(2);
          this.velocityAng.value = msg.msg.angular.z.toPrecision(2);
        });
        this._ros.add(this._subscriber);

        this._ui.log('Velocity subscriber created!');
      }).catch(err => {
        this._ui.warn('[Velocity plugin]: ' + err);
      });
    })();
  }

  destroy() {
    this._subscriber.unsubscribe();
    this.element.parentNode.removeChild(this.element);
  }
}

export { Velocity };
