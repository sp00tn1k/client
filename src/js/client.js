/**
 * sp00tn1k client
 *
 * This file contains a root class for the sp00tn1k client.
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2016, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.0
 * @license GPL-3.0-or-later
 */

import Stats from 'stats.js';

// Own modules
// import { Settings } from './settings.js';
import { UI } from './ui.js';
import { Connector } from './connector.js';
import { Renderer } from './renderer.js';
import { Resizer } from './resizer.js';

class Client {
  _renderer;
  _ui;
  _robot;
  _stats;

  constructor() {
    this._ui = new UI();
    this._renderer = new Renderer(this._ui);

    this._stats = new Stats();
    // 0: fps, 1: ms, 2: mb, 3+: custom
    this._stats.showPanel(0);
    this._stats.domElement.style.cssText = 'position:absolute;top:5px;left:5px;cursor:pointer;opacity:0.9;z-index:10000';
    // this._ui.container.appendChild(this._stats.dom);

    Resizer.update(this._ui, this._renderer);

    let resizeTimeout;
    window.addEventListener('resize', () => {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(() => { Resizer.update(this._ui, this._renderer); }, 250);
    });

    this._robot = new Connector(this._ui, this._renderer);

    this._ui.connectButton.onclick = () => {
      this._robot.connect();
    };

    this._ui.fullscreenButton.onclick = () => {
      if (!this._ui.sideColumnLeft.style.display || this._ui.sideColumnLeft.style.display === 'block') {
        this._ui.sideColumnLeft.style.display = 'none';
      } else {
        this._ui.sideColumnLeft.style.display = 'block';
      }
      window.dispatchEvent(new Event('resize')); // cause renderer to resize
    };

    window.addEventListener('keydown', e => {
      if (e.keyCode === 27) { // key ESC
        this._ui.sideColumnLeft.style.display = 'block';
      }
      window.dispatchEvent(new Event('resize')); // cause renderer to resize
    });

    // start animation
    this.loop();
  }

  /**
   * Loop is called recursively until the page end-of-live .
   * IMPORTANT: This is effectively the render loop!
   * @return {void}
   */
  loop() {
    this._stats.begin();

    this._robot.update();
    this._renderer.update();

    this._stats.end();

    // we only run animation at certain target FPS to limit resource consumption
    // setTimeout(() => {
    //     window.requestAnimationFrame(() => { this.loop() });
    // }, 1000 / Settings.Scene.FPS);

    // render as fast as possible
    window.requestAnimationFrame(() => { this.loop(); });
  }
}

export { Client };
