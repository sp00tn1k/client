/**
 * Client Settings
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2020, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.0
 * @license GPL-3.0-or-later
 */

/*
 * Renderer parameters cannot be switched on the fly.
 * More: http://threejs.org/docs/#Reference/Renderers/WebGLRenderer
 */
const Settings = {
  Messaging: {
    TwistFreq: 10
  },
  Server: {
    Peer: {
      Address: 'peer.sp00tn1k.org',
      Port: 9000,
      Path: '/'
    },
    Stun: {
      URL: 'turn.sp00tn1k.org',
      Port: 3478
    }
  },
  Robot: {
    Name: 'sp00tn1k-4',
    Camera: {
      HFOV: 360,
      VFOV: 180
    }
  },
  Common: {
    WorldSize: 30000
  },
  Interface: {
    Container: 'over_renderer',
    Canvas: 'renderer_canvas',
    StatusBar: 'status_bar',
    IDs: {
      Own: 'my_peer_id',
      Remote: 'robot_peer_id'
    },
    Videos: {
      Own: 'my_video',
      Remote: 'remote_video'
    },
    Buttons: {
      Connect: 'buttonConnect',
      Fullscreen: 'buttonFullscreen'
    },
    SideColumns: {
      Left: 'left_col',
      Right: 'right_col'
    },
    Sidebars: {
      Right: 'sidebar_right',
      Left: 'sidebar_left'
    }
  },
  Renderer: {
    // renderer params for WebGL Renderer r92dev
    Antialiasing: true,
    AspectRatio: 0.75,
    Precision: 'highp',
    // Can be "high-performance", "low-power" or "default".
    PowerPreference: 'high-performance',
    Alpha: true,
    ClearColor: '#111111',
    ClearAlpha: 1.0,
    OcclusionCulling: true,
    SortObjects: true,
    VREnabled: true,
    PreserveDrawingBuffer: true
  },
  Camera: {
    // Renderer camera
    FOV: 75,
    Near: 0.1,
    Far: 10000,
    Position: {
      x: 0,
      y: 0.01,
      z: 0
    },
    Rotation: {
      x: 0, // -Math.PI / 5,
      y: 0,
      z: 0
    }
  },
  Scene: {
    FPS: 90
  }
};

export { Settings };
