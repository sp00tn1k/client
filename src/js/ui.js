/**
 * ui is responsible for all html values and fields
 */

import { Settings } from './settings.js';

class UI {
  /**
     * find fields in the html file and load default values
     */
  constructor() {
    // find container
    this.container = document.getElementById(Settings.Interface.Container);

    this.sideColumnLeft = document.getElementById(Settings.Interface.SideColumns.Left);

    this.sidebarRight = document.getElementById(Settings.Interface.Sidebars.Right);
    this.sidebarLeft = document.getElementById(Settings.Interface.Sidebars.Left);

    // get status message field
    this.statusMessageField = document.getElementById(Settings.Interface.StatusBar);
    this.log('Loading...');

    // find canvas in the document
    this.canvas = document.getElementById(Settings.Interface.Canvas);
    this.canvasRect = this.canvas.getBoundingClientRect();

    // attach handler to connection button
    this.connectButton = document.getElementById(Settings.Interface.Buttons.Connect);
    this.fullscreenButton = document.getElementById(Settings.Interface.Buttons.Fullscreen);

    this.myPeerID = document.getElementById(Settings.Interface.IDs.Own);
    this.robotPeerID = document.getElementById(Settings.Interface.IDs.Remote);

    this.myVideo = document.getElementById(Settings.Interface.Videos.Own);
    // remote video handle
    this.remoteVideo = document.getElementById(Settings.Interface.Videos.Remote);

    this.notifyText = document.getElementById('screen_msg');
  }

  /**
     * Publish message to console log and to the status bar
     * @param {string} string message
     * @returns {undefined}
     */
  log(string) {
    // eslint-disable-next-line no-console
    console.log(string);
    this.statusMessageField.innerHTML = '[sp00tn1k] ' + string;
  }

  info(string) {
    // eslint-disable-next-line no-console
    console.info(string);
    this.statusMessageField.innerHTML = '[sp00tn1k] ' + string;
  }

  warn(string) {
    // eslint-disable-next-line no-console
    console.warn(string);
    this.statusMessageField.innerHTML = '[sp00tn1k] ' + string;
  }

  error(string) {
    // eslint-disable-next-line no-console
    console.error(string);
    this.statusMessageField.innerHTML = '[sp00tn1k] ' + string;
  }

  /* A html element displaying a message on top of the renderer canvas. No effect in VR. */
  infoScreenMsg(string) {
    let msgTimeout;
    clearTimeout(msgTimeout);
    this.notifyText.style.color = 'black';
    this.notifyText.textContent = string;
    msgTimeout = window.setTimeout(() => { this.notifyText.textContent = ''; }, 2000);
  }
}

export { UI };
