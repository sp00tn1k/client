<?php

/**
 * GIT DEPLOYMENT SCRIPT
 *
 * Used for automatically deploying websites via github or bitbucket, more deets here:
 *
 * https://gist.github.com/1809044
 * oodavid 2012
 *
 * some fixes by andrey
 */

?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
        <meta charset="UTF-8">
        <title>sp00tn1k client automatic deployment script</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
               ____________________________
              |                            |
              | sp00tn1k deployment v0.1.2 |
   .    .     |____________________________|
   |____|    /
  / ____ \
[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]
 |___==___|

Checking for updates...

<?php

ob_end_flush();
ob_implicit_flush(true);

// The commands
$commands = array(
    'echo $PWD',
    'whoami',
    'git status',
    'git pull',
    'git status',
    'git submodule sync',
    'git submodule update --init --recursive',
    'git submodule status',
);

// Run the commands for output
echo "Upgrading sp00tn1k client...\n\n";
echo " ______________________________________________________________________________ \n";
echo "|                                                                              |\n";
//chdir('/var/www/html/client');
foreach($commands AS $command){
    // Run it
    $tmp = shell_exec($command);
    // Output
    echo "<span style=\"color: #6BE234;\">\$</span><span style=\"color: #729FCF;\">{$command}\n</span>";
    echo htmlentities(($tmp)) . "\n";
}
echo "|______________________________________________________________________________|\n";
ob_implicit_flush(false);
ob_start();

?>
</pre>
</body>
</html>